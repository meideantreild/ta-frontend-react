// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getProduksiRequest: ['data'],
  getProduksiSuccess: ['payload'],
  getProduksiFailure: ['err'],
  updateProduksiRequest: ['data', 'id'],
  updateProduksiSuccess: ['payload'],
  updateProduksiFailure: ['err']
})

export const ProduksiTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  data: {},
  success: false,
  err: null
})

/* ------------- Reducers ------------- */
// we're attempting to fetch

// getProduksis
export const getProduksiRequest = (state) =>
  state.merge({ fetching: true, success: false, err: null })

export const getProduksiSuccess = (state, { payload }) =>
  state.merge({
    fetching: false,
    data: payload,
    success: true
  })

export const getProduksiFailure = (state, { err }) =>
  state.merge({
    fetching: false,
    success: false,
    err
  })

// updateProduksi
export const updateProduksiRequest = (state) =>
state.merge({ fetching: true, success: false, err: null })

export const updateProduksiSuccess = (state, { payload }) =>
state.merge({
  fetching: false,
  data: payload,
  success: true
})

export const updateProduksiFailure = (state, { err }) =>
state.merge({
  fetching: false,
  success: false,
  err
})
/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
    [Types.GET_PRODUKSI_REQUEST]: getProduksiRequest,
    [Types.GET_PRODUKSI_SUCCESS]: getProduksiSuccess,
    [Types.GET_PRODUKSI_FAILURE]: getProduksiFailure,
    [Types.UPDATE_PRODUKSI_REQUEST]: updateProduksiRequest,
    [Types.UPDATE_PRODUKSI_SUCCESS]: updateProduksiSuccess,
    [Types.UPDATE_PRODUKSI_FAILURE]: updateProduksiFailure
})