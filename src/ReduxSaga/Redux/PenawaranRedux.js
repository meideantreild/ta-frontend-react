// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getPenawaranRequest: ['data'],
  getPenawaranSuccess: ['payload'],
  getPenawaranFailure: ['err'],
  addPenawaranRequest: ['data'],
  addPenawaranSuccess: ['payload'],
  addPenawaranFailure: ['err'],
  deletePenawaranRequest: ['data'],
  deletePenawaranSuccess: ['payload'],
  deletePenawaranFailure: ['err'],
  updatePenawaranRequest: ['data', 'id'],
  updatePenawaranSuccess: ['payload'],
  updatePenawaranFailure: ['err']
})

export const PenawaranTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  data: {},
  success: false,
  err: null
})

/* ------------- Reducers ------------- */
// we're attempting to fetch

// getPenawarans
export const getPenawaranRequest = (state) =>
  state.merge({ fetching: true, success: false, err: null })

export const getPenawaranSuccess = (state, { payload }) =>
  state.merge({
    fetching: false,
    data: payload,
    success: true
  })

export const getPenawaranFailure = (state, { err }) =>
  state.merge({
    fetching: false,
    success: false,
    err
  })

// addPenawaran
export const addPenawaranRequest = (state) =>
state.merge({ fetching: true, success: false, err: null })

export const addPenawaranSuccess = (state, { payload }) =>
state.merge({
  fetching: false,
  data: payload,
  success: true
})

export const addPenawaranFailure = (state, { err }) =>
state.merge({
  fetching: false,
  success: false,
  err
})

// deletePenawaran
export const deletePenawaranRequest = (state) =>
state.merge({ fetching: true, success: false, err: null })

export const deletePenawaranSuccess = (state, { payload }) =>
state.merge({
  fetching: false,
  data: payload,
  success: true
})

export const deletePenawaranFailure = (state, { err }) =>
state.merge({
  fetching: false,
  success: false,
  err
})

// updatePenawaran
export const updatePenawaranRequest = (state) =>
state.merge({ fetching: true, success: false, err: null })

export const updatePenawaranSuccess = (state, { payload }) =>
state.merge({
  fetching: false,
  data: payload,
  success: true
})

export const updatePenawaranFailure = (state, { err }) =>
state.merge({
  fetching: false,
  success: false,
  err
})
/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
    [Types.GET_PENAWARAN_REQUEST]: getPenawaranRequest,
    [Types.GET_PENAWARAN_SUCCESS]: getPenawaranSuccess,
    [Types.GET_PENAWARAN_FAILURE]: getPenawaranFailure,
    [Types.ADD_PENAWARAN_REQUEST]: addPenawaranRequest,
    [Types.ADD_PENAWARAN_SUCCESS]: addPenawaranSuccess,
    [Types.ADD_PENAWARAN_FAILURE]: addPenawaranFailure,
    [Types.DELETE_PENAWARAN_REQUEST]: deletePenawaranRequest,
    [Types.DELETE_PENAWARAN_SUCCESS]: deletePenawaranSuccess,
    [Types.DELETE_PENAWARAN_FAILURE]: deletePenawaranFailure,
    [Types.UPDATE_PENAWARAN_REQUEST]: updatePenawaranRequest,
    [Types.UPDATE_PENAWARAN_SUCCESS]: updatePenawaranSuccess,
    [Types.UPDATE_PENAWARAN_FAILURE]: updatePenawaranFailure
})