// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  sawAddRequest: ['data'],
  sawAddSuccess: ['payload'],
  sawAddFailure: ['err'],
})

export const SAWAddTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  data: {},
  success: false,
  err: null
})

/* ------------- Reducers ------------- */
// we're attempting to fetch

// SAWAdd
export const sawAddRequest = (state) =>
state.merge({ fetching: true, success: false, err: null })

export const sawAddSuccess = (state, { payload }) =>
state.merge({
  fetching: false,
  data: payload,
  success: true
})

export const sawAddFailure = (state, { err }) =>
state.merge({
  fetching: false,
  success: false,
  err
})

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
    [Types.SAW_ADD_REQUEST]: sawAddRequest,
    [Types.SAW_ADD_SUCCESS]: sawAddSuccess,
    [Types.SAW_ADD_FAILURE]: sawAddFailure,
})