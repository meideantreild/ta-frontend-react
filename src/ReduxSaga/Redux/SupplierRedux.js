// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getSupplierRequest: ['data'],
  getSupplierSuccess: ['payload'],
  getSupplierFailure: ['err'],
  addSupplierRequest: ['data'],
  addSupplierSuccess: ['payload'],
  addSupplierFailure: ['err'],
  deleteSupplierRequest: ['data'],
  deleteSupplierSuccess: ['payload'],
  deleteSupplierFailure: ['err'],
  updateSupplierRequest: ['data', 'id'],
  updateSupplierSuccess: ['payload'],
  updateSupplierFailure: ['err']
})

export const SupplierTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  data: {},
  success: false,
  err: null
})

/* ------------- Reducers ------------- */
// we're attempting to fetch

// getSuppliers
export const getSupplierRequest = (state) =>
  state.merge({ fetching: true, success: false, err: null })

export const getSupplierSuccess = (state, { payload }) =>
  state.merge({
    fetching: false,
    data: payload,
    success: true
  })

export const getSupplierFailure = (state, { err }) =>
  state.merge({
    fetching: false,
    success: false,
    err
  })

// addSupplier
export const addSupplierRequest = (state) =>
state.merge({ fetching: true, success: false, err: null })

export const addSupplierSuccess = (state, { payload }) =>
state.merge({
  fetching: false,
  data: payload,
  success: true
})

export const addSupplierFailure = (state, { err }) =>
state.merge({
  fetching: false,
  success: false,
  err
})

// deleteSupplier
export const deleteSupplierRequest = (state) =>
state.merge({ fetching: true, success: false, err: null })

export const deleteSupplierSuccess = (state, { payload }) =>
state.merge({
  fetching: false,
  data: payload,
  success: true
})

export const deleteSupplierFailure = (state, { err }) =>
state.merge({
  fetching: false,
  success: false,
  err
})

// updateSupplier
export const updateSupplierRequest = (state) =>
state.merge({ fetching: true, success: false, err: null })

export const updateSupplierSuccess = (state, { payload }) =>
state.merge({
  fetching: false,
  data: payload,
  success: true
})

export const updateSupplierFailure = (state, { err }) =>
state.merge({
  fetching: false,
  success: false,
  err
})
/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
    [Types.GET_SUPPLIER_REQUEST]: getSupplierRequest,
    [Types.GET_SUPPLIER_SUCCESS]: getSupplierSuccess,
    [Types.GET_SUPPLIER_FAILURE]: getSupplierFailure,
    [Types.ADD_SUPPLIER_REQUEST]: addSupplierRequest,
    [Types.ADD_SUPPLIER_SUCCESS]: addSupplierSuccess,
    [Types.ADD_SUPPLIER_FAILURE]: addSupplierFailure,
    [Types.DELETE_SUPPLIER_REQUEST]: deleteSupplierRequest,
    [Types.DELETE_SUPPLIER_SUCCESS]: deleteSupplierSuccess,
    [Types.DELETE_SUPPLIER_FAILURE]: deleteSupplierFailure,
    [Types.UPDATE_SUPPLIER_REQUEST]: updateSupplierRequest,
    [Types.UPDATE_SUPPLIER_SUCCESS]: updateSupplierSuccess,
    [Types.UPDATE_SUPPLIER_FAILURE]: updateSupplierFailure
})