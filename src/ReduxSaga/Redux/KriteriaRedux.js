// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getKriteriaRequest: ['data'],
  getKriteriaSuccess: ['payload'],
  getKriteriaFailure: ['err'],
  updateKriteriaRequest: ['data', 'id'],
  updateKriteriaSuccess: ['payload'],
  updateKriteriaFailure: ['err']
})

export const KriteriaTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  data: {},
  success: false,
  err: null
})

/* ------------- Reducers ------------- */
// we're attempting to fetch

// getKriterias
export const getKriteriaRequest = (state) =>
  state.merge({ fetching: true, success: false, err: null })

export const getKriteriaSuccess = (state, { payload }) =>
  state.merge({
    fetching: false,
    data: payload,
    success: true
  })

export const getKriteriaFailure = (state, { err }) =>
  state.merge({
    fetching: false,
    success: false,
    err
  })

// updateKriteria
export const updateKriteriaRequest = (state) =>
state.merge({ fetching: true, success: false, err: null })

export const updateKriteriaSuccess = (state, { payload }) =>
state.merge({
  fetching: false,
  data: payload,
  success: true
})

export const updateKriteriaFailure = (state, { err }) =>
state.merge({
  fetching: false,
  success: false,
  err
})
/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
    [Types.GET_KRITERIA_REQUEST]: getKriteriaRequest,
    [Types.GET_KRITERIA_SUCCESS]: getKriteriaSuccess,
    [Types.GET_KRITERIA_FAILURE]: getKriteriaFailure,
    [Types.UPDATE_KRITERIA_REQUEST]: updateKriteriaRequest,
    [Types.UPDATE_KRITERIA_SUCCESS]: updateKriteriaSuccess,
    [Types.UPDATE_KRITERIA_FAILURE]: updateKriteriaFailure
})