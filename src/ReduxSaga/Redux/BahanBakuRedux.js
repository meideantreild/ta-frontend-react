// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getBahanBakuRequest: ['data'],
  getBahanBakuSuccess: ['payload'],
  getBahanBakuFailure: ['err'],
  addBahanBakuRequest: ['data'],
  addBahanBakuSuccess: ['payload'],
  addBahanBakuFailure: ['err'],
  deleteBahanBakuRequest: ['data'],
  deleteBahanBakuSuccess: ['payload'],
  deleteBahanBakuFailure: ['err'],
  updateBahanBakuRequest: ['data', 'id'],
  updateBahanBakuSuccess: ['payload'],
  updateBahanBakuFailure: ['err']
})

export const BahanBakuTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  data: {},
  success: false,
  err: null
})

/* ------------- Reducers ------------- */
// we're attempting to fetch

// getBahanBakus
export const getBahanBakuRequest = (state) =>
  state.merge({ fetching: true, success: false, err: null })

export const getBahanBakuSuccess = (state, { payload }) =>
  state.merge({
    fetching: false,
    data: payload,
    success: true
  })

export const getBahanBakuFailure = (state, { err }) =>
  state.merge({
    fetching: false,
    success: false,
    err
  })

// addBahanBaku
export const addBahanBakuRequest = (state) =>
state.merge({ fetching: true, success: false, err: null })

export const addBahanBakuSuccess = (state, { payload }) =>
state.merge({
  fetching: false,
  data: payload,
  success: true
})

export const addBahanBakuFailure = (state, { err }) =>
state.merge({
  fetching: false,
  success: false,
  err
})

// deleteBahanBaku
export const deleteBahanBakuRequest = (state) =>
state.merge({ fetching: true, success: false, err: null })

export const deleteBahanBakuSuccess = (state, { payload }) =>
state.merge({
  fetching: false,
  data: payload,
  success: true
})

export const deleteBahanBakuFailure = (state, { err }) =>
state.merge({
  fetching: false,
  success: false,
  err
})

// updateBahanBaku
export const updateBahanBakuRequest = (state) =>
state.merge({ fetching: true, success: false, err: null })

export const updateBahanBakuSuccess = (state, { payload }) =>
state.merge({
  fetching: false,
  data: payload,
  success: true
})

export const updateBahanBakuFailure = (state, { err }) =>
state.merge({
  fetching: false,
  success: false,
  err
})
/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
    [Types.GET_BAHAN_BAKU_REQUEST]: getBahanBakuRequest,
    [Types.GET_BAHAN_BAKU_SUCCESS]: getBahanBakuSuccess,
    [Types.GET_BAHAN_BAKU_FAILURE]: getBahanBakuFailure,
    [Types.ADD_BAHAN_BAKU_REQUEST]: addBahanBakuRequest,
    [Types.ADD_BAHAN_BAKU_SUCCESS]: addBahanBakuSuccess,
    [Types.ADD_BAHAN_BAKU_FAILURE]: addBahanBakuFailure,
    [Types.DELETE_BAHAN_BAKU_REQUEST]: deleteBahanBakuRequest,
    [Types.DELETE_BAHAN_BAKU_SUCCESS]: deleteBahanBakuSuccess,
    [Types.DELETE_BAHAN_BAKU_FAILURE]: deleteBahanBakuFailure,
    [Types.UPDATE_BAHAN_BAKU_REQUEST]: updateBahanBakuRequest,
    [Types.UPDATE_BAHAN_BAKU_SUCCESS]: updateBahanBakuSuccess,
    [Types.UPDATE_BAHAN_BAKU_FAILURE]: updateBahanBakuFailure
})