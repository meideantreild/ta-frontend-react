// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getStokKeluarRequest: ['data'],
  getStokKeluarSuccess: ['payload'],
  getStokKeluarFailure: ['err'],
  addStokKeluarRequest: ['data'],
  addStokKeluarSuccess: ['payload'],
  addStokKeluarFailure: ['err'],
  deleteStokKeluarRequest: ['data'],
  deleteStokKeluarSuccess: ['payload'],
  deleteStokKeluarFailure: ['err']
})

export const StokKeluarTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  data: {},
  success: false,
  err: null
})

/* ------------- Reducers ------------- */
// we're attempting to fetch

// getStokKeluars
export const getStokKeluarRequest = (state) =>
  state.merge({ fetching: true, success: false, err: null })

export const getStokKeluarSuccess = (state, { payload }) =>
  state.merge({
    fetching: false,
    data: payload,
    success: true
  })

export const getStokKeluarFailure = (state, { err }) =>
  state.merge({
    fetching: false,
    success: false,
    err
  })

// addStokKeluar
export const addStokKeluarRequest = (state) =>
state.merge({ fetching: true, success: false, err: null })

export const addStokKeluarSuccess = (state, { payload }) =>
state.merge({
  fetching: false,
  data: payload,
  success: true
})

export const addStokKeluarFailure = (state, { err }) =>
state.merge({
  fetching: false,
  success: false,
  err
})

// deleteStokKeluar
export const deleteStokKeluarRequest = (state) =>
state.merge({ fetching: true, success: false, err: null })

export const deleteStokKeluarSuccess = (state, { payload }) =>
state.merge({
  fetching: false,
  data: payload,
  success: true
})

export const deleteStokKeluarFailure = (state, { err }) =>
state.merge({
  fetching: false,
  success: false,
  err
})

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
    [Types.GET_STOK_KELUAR_REQUEST]: getStokKeluarRequest,
    [Types.GET_STOK_KELUAR_SUCCESS]: getStokKeluarSuccess,
    [Types.GET_STOK_KELUAR_FAILURE]: getStokKeluarFailure,
    [Types.ADD_STOK_KELUAR_REQUEST]: addStokKeluarRequest,
    [Types.ADD_STOK_KELUAR_SUCCESS]: addStokKeluarSuccess,
    [Types.ADD_STOK_KELUAR_FAILURE]: addStokKeluarFailure,
    [Types.DELETE_STOK_KELUAR_REQUEST]: deleteStokKeluarRequest,
    [Types.DELETE_STOK_KELUAR_SUCCESS]: deleteStokKeluarSuccess,
    [Types.DELETE_STOK_KELUAR_FAILURE]: deleteStokKeluarFailure
})