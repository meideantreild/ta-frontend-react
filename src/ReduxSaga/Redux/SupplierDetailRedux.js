// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getSupplierDetailRequest: ['data'],
  getSupplierDetailSuccess: ['payload'],
  getSupplierDetailFailure: ['err']
})

export const SupplierDetailTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  data: {},
  success: false,
  err: null
})

/* ------------- Reducers ------------- */
// we're attempting to fetch

// getSupplierDetail
export const getSupplierDetailRequest = (state) =>
  state.merge({ fetching: true, success: false, err: null })

export const getSupplierDetailSuccess = (state, { payload }) =>
  state.merge({
    fetching: false,
    data: payload,
    success: true
  })

export const getSupplierDetailFailure = (state, { err }) =>
  state.merge({
    fetching: false,
    success: false,
    err
  })

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
    [Types.GET_SUPPLIER_DETAIL_REQUEST]: getSupplierDetailRequest,
    [Types.GET_SUPPLIER_DETAIL_SUCCESS]: getSupplierDetailSuccess,
    [Types.GET_SUPPLIER_DETAIL_FAILURE]: getSupplierDetailFailure
})