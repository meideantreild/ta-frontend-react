// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  topsisAddRequest: null,
  topsisAddSuccess: ['payload'],
  topsisAddFailure: ['err'],
})

export const TopsisAddTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  data: {},
  success: false,
  err: null
})

/* ------------- Reducers ------------- */
// we're attempting to fetch

// topsisAdd
export const topsisAddRequest = (state) =>
state.merge({ fetching: true, success: false, err: null })

export const topsisAddSuccess = (state, { payload }) =>
state.merge({
  fetching: false,
  data: payload,
  success: true
})

export const topsisAddFailure = (state, { err }) =>
state.merge({
  fetching: false,
  success: false,
  err
})

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
    [Types.TOPSIS_ADD_REQUEST]: topsisAddRequest,
    [Types.TOPSIS_ADD_SUCCESS]: topsisAddSuccess,
    [Types.TOPSIS_ADD_FAILURE]: topsisAddFailure,
})