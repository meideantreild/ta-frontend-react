// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getProduksiDetailRequest: ['data'],
  getProduksiDetailSuccess: ['payload'],
  getProduksiDetailFailure: ['err']
})

export const ProduksiDetailTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  data: {},
  success: false,
  err: null
})

/* ------------- Reducers ------------- */
// we're attempting to fetch

// getProduksiDetail
export const getProduksiDetailRequest = (state) =>
  state.merge({ fetching: true, success: false, err: null })

export const getProduksiDetailSuccess = (state, { payload }) =>
  state.merge({
    fetching: false,
    data: payload,
    success: true
  })

export const getProduksiDetailFailure = (state, { err }) =>
  state.merge({
    fetching: false,
    success: false,
    err
  })

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
    [Types.GET_PRODUKSI_DETAIL_REQUEST]: getProduksiDetailRequest,
    [Types.GET_PRODUKSI_DETAIL_SUCCESS]: getProduksiDetailSuccess,
    [Types.GET_PRODUKSI_DETAIL_FAILURE]: getProduksiDetailFailure
})