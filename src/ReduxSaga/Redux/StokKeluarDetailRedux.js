// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getStokKeluarDetailRequest: ['data'],
  getStokKeluarDetailSuccess: ['payload'],
  getStokKeluarDetailFailure: ['err']
})

export const StokKeluarDetailTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  data: {},
  success: false,
  err: null
})

/* ------------- Reducers ------------- */
// we're attempting to fetch

// getStokKeluarDetail
export const getStokKeluarDetailRequest = (state) =>
  state.merge({ fetching: true, success: false, err: null })

export const getStokKeluarDetailSuccess = (state, { payload }) =>
  state.merge({
    fetching: false,
    data: payload,
    success: true
  })

export const getStokKeluarDetailFailure = (state, { err }) =>
  state.merge({
    fetching: false,
    success: false,
    err
  })

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
    [Types.GET_STOK_KELUAR_DETAIL_REQUEST]: getStokKeluarDetailRequest,
    [Types.GET_STOK_KELUAR_DETAIL_SUCCESS]: getStokKeluarDetailSuccess,
    [Types.GET_STOK_KELUAR_DETAIL_FAILURE]: getStokKeluarDetailFailure
})