// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  sawRequest: ['data'],
  sawSuccess: ['payload'],
  sawFailure: ['err'],
})

export const SAWTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  data: {},
  success: false,
  err: null
})

/* ------------- Reducers ------------- */
// we're attempting to fetch

// SAW
export const sawRequest = (state) =>
  state.merge({ fetching: true, success: false, err: null })

export const sawSuccess = (state, { payload }) =>
  state.merge({
    fetching: false,
    data: payload,
    success: true
  })

export const sawFailure = (state, { err }) =>
  state.merge({
    fetching: false,
    success: false,
    err
  })

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
    [Types.SAW_REQUEST]: sawRequest,
    [Types.SAW_SUCCESS]: sawSuccess,
    [Types.SAW_FAILURE]: sawFailure,
})