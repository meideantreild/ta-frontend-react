// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getStokMasukDetailRequest: ['data'],
  getStokMasukDetailSuccess: ['payload'],
  getStokMasukDetailFailure: ['err']
})

export const StokMasukDetailTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  data: {},
  success: false,
  err: null
})

/* ------------- Reducers ------------- */
// we're attempting to fetch

// getStokMasukDetail
export const getStokMasukDetailRequest = (state) =>
  state.merge({ fetching: true, success: false, err: null })

export const getStokMasukDetailSuccess = (state, { payload }) =>
  state.merge({
    fetching: false,
    data: payload,
    success: true
  })

export const getStokMasukDetailFailure = (state, { err }) =>
  state.merge({
    fetching: false,
    success: false,
    err
  })

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
    [Types.GET_STOK_MASUK_DETAIL_REQUEST]: getStokMasukDetailRequest,
    [Types.GET_STOK_MASUK_DETAIL_SUCCESS]: getStokMasukDetailSuccess,
    [Types.GET_STOK_MASUK_DETAIL_FAILURE]: getStokMasukDetailFailure
})