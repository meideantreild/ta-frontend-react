// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  topsisRequest: ['data'],
  topsisSuccess: ['payload'],
  topsisFailure: ['err'],
})

export const TopsisTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  data: {},
  success: false,
  err: null
})

/* ------------- Reducers ------------- */
// we're attempting to fetch

// Topsis
export const topsisRequest = (state) =>
  state.merge({ fetching: true, success: false, err: null })

export const topsisSuccess = (state, { payload }) =>
  state.merge({
    fetching: false,
    data: payload,
    success: true
  })

export const topsisFailure = (state, { err }) =>
  state.merge({
    fetching: false,
    success: false,
    err
  })

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
    [Types.TOPSIS_REQUEST]: topsisRequest,
    [Types.TOPSIS_SUCCESS]: topsisSuccess,
    [Types.TOPSIS_FAILURE]: topsisFailure,
})