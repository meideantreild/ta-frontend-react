// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getBahanBakuDetailRequest: ['data'],
  getBahanBakuDetailSuccess: ['payload'],
  getBahanBakuDetailFailure: ['err']
})

export const BahanBakuDetailTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  data: {},
  success: false,
  err: null
})

/* ------------- Reducers ------------- */
// we're attempting to fetch

// getBahanBakuDetail
export const getBahanBakuDetailRequest = (state) =>
  state.merge({ fetching: true, success: false, err: null })

export const getBahanBakuDetailSuccess = (state, { payload }) =>
  state.merge({
    fetching: false,
    data: payload,
    success: true
  })

export const getBahanBakuDetailFailure = (state, { err }) =>
  state.merge({
    fetching: false,
    success: false,
    err
  })

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
    [Types.GET_BAHAN_BAKU_DETAIL_REQUEST]: getBahanBakuDetailRequest,
    [Types.GET_BAHAN_BAKU_DETAIL_SUCCESS]: getBahanBakuDetailSuccess,
    [Types.GET_BAHAN_BAKU_DETAIL_FAILURE]: getBahanBakuDetailFailure
})