// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getStokMasukRequest: ['data'],
  getStokMasukSuccess: ['payload'],
  getStokMasukFailure: ['err'],
  addStokMasukRequest: ['data'],
  addStokMasukSuccess: ['payload'],
  addStokMasukFailure: ['err'],
  deleteStokMasukRequest: ['data'],
  deleteStokMasukSuccess: ['payload'],
  deleteStokMasukFailure: ['err']
})

export const StokMasukTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  data: {},
  success: false,
  err: null
})

/* ------------- Reducers ------------- */
// we're attempting to fetch

// getStokMasuks
export const getStokMasukRequest = (state) =>
  state.merge({ fetching: true, success: false, err: null })

export const getStokMasukSuccess = (state, { payload }) =>
  state.merge({
    fetching: false,
    data: payload,
    success: true
  })

export const getStokMasukFailure = (state, { err }) =>
  state.merge({
    fetching: false,
    success: false,
    err
  })

// addStokMasuk
export const addStokMasukRequest = (state) =>
state.merge({ fetching: true, success: false, err: null })

export const addStokMasukSuccess = (state, { payload }) =>
state.merge({
  fetching: false,
  data: payload,
  success: true
})

export const addStokMasukFailure = (state, { err }) =>
state.merge({
  fetching: false,
  success: false,
  err
})

// deleteStokMasuk
export const deleteStokMasukRequest = (state) =>
state.merge({ fetching: true, success: false, err: null })

export const deleteStokMasukSuccess = (state, { payload }) =>
state.merge({
  fetching: false,
  data: payload,
  success: true
})

export const deleteStokMasukFailure = (state, { err }) =>
state.merge({
  fetching: false,
  success: false,
  err
})

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
    [Types.GET_STOK_MASUK_REQUEST]: getStokMasukRequest,
    [Types.GET_STOK_MASUK_SUCCESS]: getStokMasukSuccess,
    [Types.GET_STOK_MASUK_FAILURE]: getStokMasukFailure,
    [Types.ADD_STOK_MASUK_REQUEST]: addStokMasukRequest,
    [Types.ADD_STOK_MASUK_SUCCESS]: addStokMasukSuccess,
    [Types.ADD_STOK_MASUK_FAILURE]: addStokMasukFailure,
    [Types.DELETE_STOK_MASUK_REQUEST]: deleteStokMasukRequest,
    [Types.DELETE_STOK_MASUK_SUCCESS]: deleteStokMasukSuccess,
    [Types.DELETE_STOK_MASUK_FAILURE]: deleteStokMasukFailure,
})