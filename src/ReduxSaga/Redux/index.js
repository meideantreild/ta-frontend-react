import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'

// Reducers
import { reducer as routeRedux } from './RouteRedux'
import { reducer as loginRedux } from  './LoginRedux'
import { reducer as logoutRedux } from  './LogoutRedux'
import { reducer as bahanBakuRedux } from './BahanBakuRedux'
import { reducer as bahanBakuDetailRedux } from './BahanBakuDetailRedux'
import { reducer as stokMasukRedux } from './StokMasukRedux'
import { reducer as stokMasukDetailRedux } from './StokMasukDetailRedux'
import { reducer as stokKeluarRedux } from './StokKeluarRedux'
import { reducer as stokKeluarDetailRedux } from './StokKeluarDetailRedux'
import { reducer as produksiRedux } from './ProduksiRedux'
import { reducer as produksiDetailRedux } from './ProduksiDetailRedux'
import { reducer as supplierRedux } from './SupplierRedux'
import { reducer as supplierDetailRedux } from './SupplierDetailRedux'
import { reducer as penawaranRedux } from './PenawaranRedux'
import { reducer as penawaranDetailRedux } from './PenawaranDetailRedux'
import { reducer as kriteriaRedux } from './KriteriaRedux'
import { reducer as kriteriaDetailRedux } from './KriteriaDetailRedux'
import { reducer as topsisRedux } from './TopsisRedux'
import { reducer as topsisAddRedux } from './TopsisAddRedux'
import { reducer as sawRedux } from './SAWRedux'
import { reducer as sawAddRedux } from './SAWAddRedux'

import ConfigureStore from './configureStore'

const initialState = {}

const rootReducer = (history) => combineReducers({
  router: connectRouter(history),
  routeState: routeRedux,
  authState: loginRedux,
  logoutState: logoutRedux,
  bahanBakuState: bahanBakuRedux,
  bahanBakuDetailState: bahanBakuDetailRedux,
  produksiState: produksiRedux,
  produksiDetailState: produksiDetailRedux,
  stokMasukState: stokMasukRedux,
  stokMasukDetailState: stokMasukDetailRedux,
  stokKeluarState: stokKeluarRedux,
  stokKeluarDetailState: stokKeluarDetailRedux,
  supplierState: supplierRedux,
  supplierDetailState: supplierDetailRedux,
  penawaranState: penawaranRedux,
  penawaranDetailState: penawaranDetailRedux,
  kriteriaState: kriteriaRedux,
  kriteriaDetailState: kriteriaDetailRedux,
  topsisState: topsisRedux,
  topsisAddState: topsisAddRedux,
  sawState: sawRedux,
  sawAddState: sawAddRedux
})

export default ConfigureStore(rootReducer, initialState)