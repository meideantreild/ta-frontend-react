// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getPenawaranDetailRequest: ['data'],
  getPenawaranDetailSuccess: ['payload'],
  getPenawaranDetailFailure: ['err']
})

export const PenawaranDetailTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  data: {},
  success: false,
  err: null
})

/* ------------- Reducers ------------- */
// we're attempting to fetch

// getPenawaranDetail
export const getPenawaranDetailRequest = (state) =>
  state.merge({ fetching: true, success: false, err: null })

export const getPenawaranDetailSuccess = (state, { payload }) =>
  state.merge({
    fetching: false,
    data: payload,
    success: true
  })

export const getPenawaranDetailFailure = (state, { err }) =>
  state.merge({
    fetching: false,
    success: false,
    err
  })

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
    [Types.GET_PENAWARAN_DETAIL_REQUEST]: getPenawaranDetailRequest,
    [Types.GET_PENAWARAN_DETAIL_SUCCESS]: getPenawaranDetailSuccess,
    [Types.GET_PENAWARAN_DETAIL_FAILURE]: getPenawaranDetailFailure
})