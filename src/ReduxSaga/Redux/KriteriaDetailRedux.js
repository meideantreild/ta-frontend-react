// @flow
import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getKriteriaDetailRequest: ['data'],
  getKriteriaDetailSuccess: ['payload'],
  getKriteriaDetailFailure: ['err']
})

export const KriteriaDetailTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  data: {},
  success: false,
  err: null
})

/* ------------- Reducers ------------- */
// we're attempting to fetch

// getKriteriaDetail
export const getKriteriaDetailRequest = (state) =>
  state.merge({ fetching: true, success: false, err: null })

export const getKriteriaDetailSuccess = (state, { payload }) =>
  state.merge({
    fetching: false,
    data: payload,
    success: true
  })

export const getKriteriaDetailFailure = (state, { err }) =>
  state.merge({
    fetching: false,
    success: false,
    err
  })

/* ------------- Hookup Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
    [Types.GET_KRITERIA_DETAIL_REQUEST]: getKriteriaDetailRequest,
    [Types.GET_KRITERIA_DETAIL_SUCCESS]: getKriteriaDetailSuccess,
    [Types.GET_KRITERIA_DETAIL_FAILURE]: getKriteriaDetailFailure
})