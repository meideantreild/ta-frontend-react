import { put, take, fork, call } from 'redux-saga/effects'
import localforage from 'localforage'
import { END } from 'redux-saga'
import { message } from 'antd'

// Redux stuffs
import LoginActions, { LoginTypes } from '../Redux/LoginRedux'
import LogoutActions, { LogoutTypes } from '../Redux/LogoutRedux'
import RouteActions, { RouteTypes } from '../Redux/RouteRedux'
import { isEmpty } from 'lodash'

export function * loginRequest (api) {
    let action = yield take(LoginTypes.LOGIN_REQUEST)
    while (action !== END) {
        yield fork(loginRequestAPI, api, action)
        action = yield take(LoginTypes.LOGIN_REQUEST)
    }
}

export function * loginRequestAPI (api, {data}) {
    try {
        if (data !== '') {
            const response = yield call(api.login, data)
            console.log(response)
            if (response.ok) {
                yield localforage.setItem('token', response.data.data.password)
                return yield put(LoginActions.loginSuccess(response.data.data))
            } else {
                return yield put(LoginActions.loginFailure(response.data))
            }
        }
    } catch (err) {
        return yield put(LoginActions.loginFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

export function * logoutRequest () {
    let action = yield take(LogoutTypes.LOGOUT_REQUEST)
    while (action !== END) {
        yield fork(logoutRequestRoute, action)
        action = yield take(LogoutTypes.LOGOUT_REQUEST)
    }
}

export function * logoutRequestRoute () {
    const token = yield localforage.setItem('token', '')
    yield put(RouteActions.routeSuccess({ token: token }))
}