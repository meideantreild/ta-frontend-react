import { put, take, fork } from 'redux-saga/effects'
import { END } from 'redux-saga'
import localforage from 'localforage'

// Redux Stuffs
import RouteActions, { RouteTypes } from '../Redux/RouteRedux'

export function * routeRequest () {
    let action = yield take(RouteTypes.ROUTE_REQUEST)
    while (action !== END) {
        yield fork(authenticateRoute, action)
        action = yield take(RouteTypes.ROUTE_REQUEST)
    }
}

export function * authenticateRoute (data) {
    const token = yield localforage.getItem('token')
    if (token) {
        yield put(RouteActions.routeSuccess({ token: token }))
    } else {
        yield put(RouteActions.routeSuccess({ token: 'NO_TOKEN' }))
    }
}