import { put, take, fork, call } from 'redux-saga/effects'
import { END } from 'redux-saga'

import ProduksiActions, { ProduksiTypes } from '../Redux/ProduksiRedux'
import ProduksiDetailActions, { ProduksiDetailTypes } from '../Redux/ProduksiDetailRedux'

// GET Produksi
export function * getProduksiRequest (api) {
    let action = yield take(ProduksiTypes.GET_PRODUKSI_REQUEST)
    while (action !== END) {
        yield fork(getProduksiRequestAPI, api, action)
        action = yield take(ProduksiTypes.GET_PRODUKSI_REQUEST)
    }
}

export function * getProduksiRequestAPI (api, {data}) {
    try {
        const response = yield call(api.getProduksi, data)
        if (response.ok) {
            return yield put(ProduksiActions.getProduksiSuccess(response.data))
        } else {
            return yield put(ProduksiActions.getProduksiFailure(response.data))
        }
    } catch (err) {
        return yield put(ProduksiActions.getProduksiFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// GET Produksi DETAIL
export function * getProduksiDetailRequest (api) {
    let action = yield take(ProduksiDetailTypes.GET_PRODUKSI_DETAIL_REQUEST)
    while (action !== END) {
        yield fork(getProduksiDetailRequestAPI, api, action)
        action = yield take(ProduksiDetailTypes.GET_PRODUKSI_DETAIL_REQUEST)
    }
}

export function * getProduksiDetailRequestAPI (api, {data}) {
    try {
        const response = yield call(api.getProduksiDetail, data)
        if (response.ok) {
            return yield put(ProduksiDetailActions.getProduksiDetailSuccess(response.data.data))
        } else {
            return yield put(ProduksiDetailActions.getProduksiDetailFailure(response.data))
        }
    } catch (err) {
        return yield put(ProduksiDetailActions.getProduksiDetailFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// UPDATE Produksi
export function * updateProduksiRequest (api) {
    let action = yield take(ProduksiTypes.UPDATE_PRODUKSI_REQUEST)
    while (action !== END) {    
        yield fork(updateProduksiRequestAPI, api, action)
        action = yield take(ProduksiTypes.UPDATE_PRODUKSI_REQUEST)
    }
}

export function * updateProduksiRequestAPI (api, {data, id}) {
    try {
        const response = yield call(api.updateProduksi, data, id)
        if (response.ok) {
            const response = yield call(api.getProduksi, 1)
            return yield put(ProduksiActions.updateProduksiSuccess(response.data))
        } else {
            return yield put(ProduksiActions.updateProduksiFailure(response.data))
        }
    } catch (err) {
        return yield put(ProduksiActions.updateProduksiFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}