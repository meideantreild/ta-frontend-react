import { put, take, fork, call } from 'redux-saga/effects'
import { END } from 'redux-saga'

import StokMasukActions, { StokMasukTypes } from '../Redux/StokMasukRedux'
import StokMasukDetailActions, { StokMasukDetailTypes } from '../Redux/StokMasukDetailRedux'

// GET StokMasuk
export function * getStokMasukRequest (api) {
    let action = yield take(StokMasukTypes.GET_STOK_MASUK_REQUEST)
    while (action !== END) {
        yield fork(getStokMasukRequestAPI, api, action)
        action = yield take(StokMasukTypes.GET_STOK_MASUK_REQUEST)
    }
}

export function * getStokMasukRequestAPI (api, {data}) {
    try {
        const response = yield call(api.getStokMasuk, data)
        if (response.ok) {
            return yield put(StokMasukActions.getStokMasukSuccess(response.data))
        } else {
            return yield put(StokMasukActions.getStokMasukFailure(response.data))
        }
    } catch (err) {
        return yield put(StokMasukActions.getStokMasukFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// GET StokMasuk DETAIL
export function * getStokMasukDetailRequest (api) {
    let action = yield take(StokMasukDetailTypes.GET_STOK_MASUK_DETAIL_REQUEST)
    while (action !== END) {
        yield fork(getStokMasukDetailRequestAPI, api, action)
        action = yield take(StokMasukDetailTypes.GET_STOK_MASUK_DETAIL_REQUEST)
    }
}

export function * getStokMasukDetailRequestAPI (api, {data}) {
    try {
        const response = yield call(api.getStokMasukDetail, data)
        if (response.ok) {
            return yield put(StokMasukDetailActions.getStokMasukDetailSuccess(response.data.data))
        } else {
            return yield put(StokMasukDetailActions.getStokMasukDetailFailure(response.data))
        }
    } catch (err) {
        return yield put(StokMasukDetailActions.getStokMasukDetailFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// ADD StokMasuk
export function * addStokMasukRequest (api) {
    let action = yield take(StokMasukTypes.ADD_STOK_MASUK_REQUEST)
    while (action !== END) {
        yield fork(addStokMasukRequestAPI, api, action)
        action = yield take(StokMasukTypes.ADD_STOK_MASUK_REQUEST)
    }
}

export function * addStokMasukRequestAPI (api, {data}) {
    try {
        const response = yield call(api.addStokMasuk, data)
        if (response.ok) {
            const response = yield call(api.getStokMasuk, 1)
            return yield put(StokMasukActions.addStokMasukSuccess(response.data))
        } else {
            return yield put(StokMasukActions.addStokMasukFailure(response.data))
        }
    } catch (err) {
        return yield put(StokMasukActions.addStokMasukFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// DELETE StokMasuk
export function * deleteStokMasukRequest (api) {
    let action = yield take(StokMasukTypes.DELETE_STOK_MASUK_REQUEST)
    while (action !== END) {
        yield fork(deleteStokMasukRequestAPI, api, action)
        action = yield take(StokMasukTypes.DELETE_STOK_MASUK_REQUEST)
    }
}

export function * deleteStokMasukRequestAPI (api, {data}) {
    try {
        const response = yield call(api.deleteStokMasuk, data)
        if (response.ok) {
            const response = yield call(api.getStokMasuk, 1)
            return yield put(StokMasukActions.deleteStokMasukSuccess(response.data))
        } else {
            return yield put(StokMasukActions.deleteStokMasukFailure(response.data))
        }
    } catch (err) {
        return yield put(StokMasukActions.deleteStokMasukFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}