import { fork, all } from 'redux-saga/effects'
import localforage from 'localforage'

// sagas
import { routeRequest } from './RouteSaga'
import { loginRequest, logoutRequest } from './AuthSaga'
import {
  getBahanBakuRequest,
  getBahanBakuDetailRequest,
  addBahanBakuRequest,
  deleteBahanBakuRequest,
  updateBahanBakuRequest
} from './BahanBakuSaga'
import {
  getStokMasukRequest,
  getStokMasukDetailRequest,
  addStokMasukRequest,
  deleteStokMasukRequest
} from './StokMasukSaga'
import {
  getStokKeluarRequest,
  getStokKeluarDetailRequest,
  addStokKeluarRequest,
  deleteStokKeluarRequest
} from './StokKeluarSaga.js'
import {
  getProduksiRequest,
  getProduksiDetailRequest,
  updateProduksiRequest
} from './ProduksiSaga'
import {
  getSupplierRequest,
  getSupplierDetailRequest,
  addSupplierRequest,
  deleteSupplierRequest,
  updateSupplierRequest
} from './SupplierSaga'
import {
  getPenawaranRequest,
  getPenawaranDetailRequest,
  addPenawaranRequest,
  deletePenawaranRequest,
  updatePenawaranRequest
} from './PenawaranSaga'
import {
  getKriteriaRequest,
  getKriteriaDetailRequest,
  updateKriteriaRequest
} from './KriteriaSaga'
import {
  topsisRequest,
  topsisAddRequest
} from './TopsisSaga'
import {
  sawRequest,
  sawAddRequest
} from './SAWSaga'

// api
import API from '../../Services/Api'

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = API.create()
const getToken = () => localforage.getItem('token')

function * routeAuthSaga () {
  yield all([fork(routeRequest, api)])
}

function * authSaga () {
  yield all([
    fork(loginRequest, api),
    fork(logoutRequest, api)
  ])
}

function * bahanBakuSaga () {
  yield all([
    fork(getBahanBakuRequest, api),
    fork(getBahanBakuDetailRequest, api),
    fork(addBahanBakuRequest, api),
    fork(deleteBahanBakuRequest, api),
    fork(updateBahanBakuRequest, api)
  ])
}

function * stokMasukSaga () {
  yield all([
    fork(getStokMasukRequest, api),
    fork(getStokMasukDetailRequest, api),
    fork(addStokMasukRequest, api),
    fork(deleteStokMasukRequest, api)
  ])
}

function * stokKeluarSaga () {
  yield all([
    fork(getStokKeluarRequest, api),
    fork(getStokKeluarDetailRequest, api),
    fork(addStokKeluarRequest, api),
    fork(deleteStokKeluarRequest, api)
  ])
}

function * produksiSaga () {
  yield all([
    fork(getProduksiRequest, api),
    fork(getProduksiDetailRequest, api),
    fork(updateProduksiRequest, api)
  ])
}

function * supplierSaga () {
  yield all([
    fork(getSupplierRequest, api),
    fork(getSupplierDetailRequest, api),
    fork(addSupplierRequest, api),
    fork(deleteSupplierRequest, api),
    fork(updateSupplierRequest, api)
  ])
}

function * penawaranSaga () {
  yield all([
    fork(getPenawaranRequest, api),
    fork(getPenawaranDetailRequest, api),
    fork(addPenawaranRequest, api),
    fork(deletePenawaranRequest, api),
    fork(updatePenawaranRequest, api)
  ])
}

function * kriteriaSaga () {
  yield all([
    fork(getKriteriaRequest, api),
    fork(getKriteriaDetailRequest, api),
    fork(updateKriteriaRequest, api)
  ])
}

function * topsisSaga () {
  yield all([
    fork(topsisRequest, api),
    fork(topsisAddRequest, api)
  ])
}

function * sawSaga () {
  yield all([
    fork(sawRequest, api),
    fork(sawAddRequest, api)
  ])
}

export default function * root () {
  yield all([
    fork(routeAuthSaga),
    fork(authSaga),
    fork(bahanBakuSaga),
    fork(stokMasukSaga),
    fork(stokKeluarSaga),
    fork(produksiSaga),
    fork(supplierSaga),
    fork(penawaranSaga),
    fork(kriteriaSaga),
    fork(topsisSaga),
    fork(sawSaga)
  ])
}