import { put, take, fork, call } from 'redux-saga/effects'
import { END } from 'redux-saga'

import PenawaranActions, { PenawaranTypes } from '../Redux/PenawaranRedux'
import PenawaranDetailActions, { PenawaranDetailTypes } from '../Redux/PenawaranDetailRedux'

// GET Penawaran
export function * getPenawaranRequest (api) {
    let action = yield take(PenawaranTypes.GET_PENAWARAN_REQUEST)
    while (action !== END) {
        yield fork(getPenawaranRequestAPI, api, action)
        action = yield take(PenawaranTypes.GET_PENAWARAN_REQUEST)
    }
}

export function * getPenawaranRequestAPI (api, {data}) {
    try {
        const response = yield call(api.getPenawaran, data)
        if (response.ok) {
            return yield put(PenawaranActions.getPenawaranSuccess(response.data))
        } else {
            return yield put(PenawaranActions.getPenawaranFailure(response.data))
        }
    } catch (err) {
        return yield put(PenawaranActions.getPenawaranFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// GET Penawaran DETAIL
export function * getPenawaranDetailRequest (api) {
    let action = yield take(PenawaranDetailTypes.GET_PENAWARAN_DETAIL_REQUEST)
    while (action !== END) {
        yield fork(getPenawaranDetailRequestAPI, api, action)
        action = yield take(PenawaranDetailTypes.GET_PENAWARAN_DETAIL_REQUEST)
    }
}

export function * getPenawaranDetailRequestAPI (api, {data}) {
    try {
        const response = yield call(api.getPenawaranDetail, data)
        if (response.ok) {
            return yield put(PenawaranDetailActions.getPenawaranDetailSuccess(response.data.data))
        } else {
            return yield put(PenawaranDetailActions.getPenawaranDetailFailure(response.data))
        }
    } catch (err) {
        return yield put(PenawaranDetailActions.getPenawaranDetailFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// ADD Penawaran
export function * addPenawaranRequest (api) {
    let action = yield take(PenawaranTypes.ADD_PENAWARAN_REQUEST)
    while (action !== END) {
        yield fork(addPenawaranRequestAPI, api, action)
        action = yield take(PenawaranTypes.ADD_PENAWARAN_REQUEST)
    }
}

export function * addPenawaranRequestAPI (api, {data}) {
    try {
        const response = yield call(api.addPenawaran, data)
        if (response.ok) {
            const response = yield call(api.getPenawaran, 1)
            return yield put(PenawaranActions.addPenawaranSuccess(response.data))
        } else {
            return yield put(PenawaranActions.addPenawaranFailure(response.data))
        }
    } catch (err) {
        return yield put(PenawaranActions.addPenawaranFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// DELETE Penawaran
export function * deletePenawaranRequest (api) {
    let action = yield take(PenawaranTypes.DELETE_PENAWARAN_REQUEST)
    while (action !== END) {
        yield fork(deletePenawaranRequestAPI, api, action)
        action = yield take(PenawaranTypes.DELETE_PENAWARAN_REQUEST)
    }
}

export function * deletePenawaranRequestAPI (api, {data}) {
    try {
        const response = yield call(api.deletePenawaran, data)
        if (response.ok) {
            const response = yield call(api.getPenawaran, 1)
            return yield put(PenawaranActions.deletePenawaranSuccess(response.data))
        } else {
            return yield put(PenawaranActions.deletePenawaranFailure(response.data))
        }
    } catch (err) {
        return yield put(PenawaranActions.deletePenawaranFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// UPDATE Penawaran
export function * updatePenawaranRequest (api) {
    let action = yield take(PenawaranTypes.UPDATE_PENAWARAN_REQUEST)
    while (action !== END) {
        yield fork(updatePenawaranRequestAPI, api, action)
        action = yield take(PenawaranTypes.UPDATE_PENAWARAN_REQUEST)
    }
}

export function * updatePenawaranRequestAPI (api, {data, id}) {
    try {
        const response = yield call(api.updatePenawaran, data, id)
        if (response.ok) {
            const response = yield call(api.getPenawaran, 1)
            return yield put(PenawaranActions.updatePenawaranSuccess(response.data))
        } else {
            return yield put(PenawaranActions.updatePenawaranFailure(response.data))
        }
    } catch (err) {
        return yield put(PenawaranActions.updatePenawaranFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}