import { put, take, fork, call } from 'redux-saga/effects'
import { END } from 'redux-saga'

import KriteriaActions, { KriteriaTypes } from '../Redux/KriteriaRedux'
import KriteriaDetailActions, { KriteriaDetailTypes } from '../Redux/KriteriaDetailRedux'

// GET Kriteria
export function * getKriteriaRequest (api) {
    let action = yield take(KriteriaTypes.GET_KRITERIA_REQUEST)
    while (action !== END) {
        yield fork(getKriteriaRequestAPI, api, action)
        action = yield take(KriteriaTypes.GET_KRITERIA_REQUEST)
    }
}

export function * getKriteriaRequestAPI (api, {data}) {
    try {
        const response = yield call(api.getKriteria, data)
        if (response.ok) {
            return yield put(KriteriaActions.getKriteriaSuccess(response.data))
        } else {
            return yield put(KriteriaActions.getKriteriaFailure(response.data))
        }
    } catch (err) {
        return yield put(KriteriaActions.getKriteriaFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// GET Kriteria DETAIL
export function * getKriteriaDetailRequest (api) {
    let action = yield take(KriteriaDetailTypes.GET_KRITERIA_DETAIL_REQUEST)
    while (action !== END) {
        yield fork(getKriteriaDetailRequestAPI, api, action)
        action = yield take(KriteriaDetailTypes.GET_KRITERIA_DETAIL_REQUEST)
    }
}

export function * getKriteriaDetailRequestAPI (api, {data}) {
    try {
        const response = yield call(api.getKriteriaDetail, data)
        if (response.ok) {
            return yield put(KriteriaDetailActions.getKriteriaDetailSuccess(response.data.data))
        } else {
            return yield put(KriteriaDetailActions.getKriteriaDetailFailure(response.data))
        }
    } catch (err) {
        return yield put(KriteriaDetailActions.getKriteriaDetailFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// UPDATE Kriteria
export function * updateKriteriaRequest (api) {
    let action = yield take(KriteriaTypes.UPDATE_KRITERIA_REQUEST)
    while (action !== END) {    
        yield fork(updateKriteriaRequestAPI, api, action)
        action = yield take(KriteriaTypes.UPDATE_KRITERIA_REQUEST)
    }
}

export function * updateKriteriaRequestAPI (api, {data, id}) {
    try {
        const response = yield call(api.updateKriteria, data, id)
        if (response.ok) {
            const response = yield call(api.getKriteria, 1)
            return yield put(KriteriaActions.updateKriteriaSuccess(response.data))
        } else {
            return yield put(KriteriaActions.updateKriteriaFailure(response.data))
        }
    } catch (err) {
        return yield put(KriteriaActions.updateKriteriaFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}