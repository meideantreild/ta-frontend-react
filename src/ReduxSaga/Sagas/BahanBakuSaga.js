import { put, take, fork, call } from 'redux-saga/effects'
import { END } from 'redux-saga'

import BahanBakuActions, { BahanBakuTypes } from '../Redux/BahanBakuRedux'
import BahanBakuDetailActions, { BahanBakuDetailTypes } from '../Redux/BahanBakuDetailRedux'

// GET BahanBaku
export function * getBahanBakuRequest (api) {
    let action = yield take(BahanBakuTypes.GET_BAHAN_BAKU_REQUEST)
    while (action !== END) {
        yield fork(getBahanBakuRequestAPI, api, action)
        action = yield take(BahanBakuTypes.GET_BAHAN_BAKU_REQUEST)
    }
}

export function * getBahanBakuRequestAPI (api, {data}) {
    try {
        const response = yield call(api.getBahanBaku, data)
        if (response.ok) {
            return yield put(BahanBakuActions.getBahanBakuSuccess(response.data))
        } else {
            return yield put(BahanBakuActions.getBahanBakuFailure(response.data))
        }
    } catch (err) {
        return yield put(BahanBakuActions.getBahanBakuFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// GET BahanBaku DETAIL
export function * getBahanBakuDetailRequest (api) {
    let action = yield take(BahanBakuDetailTypes.GET_BAHAN_BAKU_DETAIL_REQUEST)
    while (action !== END) {
        yield fork(getBahanBakuDetailRequestAPI, api, action)
        action = yield take(BahanBakuDetailTypes.GET_BAHAN_BAKU_DETAIL_REQUEST)
    }
}

export function * getBahanBakuDetailRequestAPI (api, {data}) {
    try {
        const response = yield call(api.getBahanBakuDetail, data)
        if (response.ok) {
            return yield put(BahanBakuDetailActions.getBahanBakuDetailSuccess(response.data.data))
        } else {
            return yield put(BahanBakuDetailActions.getBahanBakuDetailFailure(response.data))
        }
    } catch (err) {
        return yield put(BahanBakuDetailActions.getBahanBakuDetailFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// ADD BahanBaku
export function * addBahanBakuRequest (api) {
    let action = yield take(BahanBakuTypes.ADD_BAHAN_BAKU_REQUEST)
    while (action !== END) {
        yield fork(addBahanBakuRequestAPI, api, action)
        action = yield take(BahanBakuTypes.ADD_BAHAN_BAKU_REQUEST)
    }
}

export function * addBahanBakuRequestAPI (api, {data}) {
    try {
        const response = yield call(api.addBahanBaku, data)
        if (response.ok) {
            const response = yield call(api.getBahanBaku, 1)
            return yield put(BahanBakuActions.addBahanBakuSuccess(response.data))
        } else {
            return yield put(BahanBakuActions.addBahanBakuFailure(response.data))
        }
    } catch (err) {
        return yield put(BahanBakuActions.addBahanBakuFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// DELETE BahanBaku
export function * deleteBahanBakuRequest (api) {
    let action = yield take(BahanBakuTypes.DELETE_BAHAN_BAKU_REQUEST)
    while (action !== END) {
        yield fork(deleteBahanBakuRequestAPI, api, action)
        action = yield take(BahanBakuTypes.DELETE_BAHAN_BAKU_REQUEST)
    }
}

export function * deleteBahanBakuRequestAPI (api, {data}) {
    try {
        const response = yield call(api.deleteBahanBaku, data)
        if (response.ok) {
            const response = yield call(api.getBahanBaku, 1)
            return yield put(BahanBakuActions.deleteBahanBakuSuccess(response.data))
        } else {
            return yield put(BahanBakuActions.deleteBahanBakuFailure(response.data))
        }
    } catch (err) {
        return yield put(BahanBakuActions.deleteBahanBakuFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// UPDATE BahanBaku
export function * updateBahanBakuRequest (api) {
    let action = yield take(BahanBakuTypes.UPDATE_BAHAN_BAKU_REQUEST)
    while (action !== END) {
        yield fork(updateBahanBakuRequestAPI, api, action)
        action = yield take(BahanBakuTypes.UPDATE_BAHAN_BAKU_REQUEST)
    }
}

export function * updateBahanBakuRequestAPI (api, {data, id}) {
    try {
        const response = yield call(api.updateBahanBaku, data, id)
        if (response.ok) {
            const response = yield call(api.getBahanBaku, 1)
            return yield put(BahanBakuActions.updateBahanBakuSuccess(response.data))
        } else {
            return yield put(BahanBakuActions.updateBahanBakuFailure(response.data))
        }
    } catch (err) {
        return yield put(BahanBakuActions.updateBahanBakuFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}