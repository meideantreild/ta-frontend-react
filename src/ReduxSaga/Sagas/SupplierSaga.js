import { put, take, fork, call } from 'redux-saga/effects'
import { END } from 'redux-saga'

import SupplierActions, { SupplierTypes } from '../Redux/SupplierRedux'
import SupplierDetailActions, { SupplierDetailTypes } from '../Redux/SupplierDetailRedux'

// GET Supplier
export function * getSupplierRequest (api) {
    let action = yield take(SupplierTypes.GET_SUPPLIER_REQUEST)
    while (action !== END) {
        yield fork(getSupplierRequestAPI, api, action)
        action = yield take(SupplierTypes.GET_SUPPLIER_REQUEST)
    }
}

export function * getSupplierRequestAPI (api, {data}) {
    try {
        const response = yield call(api.getSupplier, data)
        if (response.ok) {
            return yield put(SupplierActions.getSupplierSuccess(response.data))
        } else {
            return yield put(SupplierActions.getSupplierFailure(response.data))
        }
    } catch (err) {
        return yield put(SupplierActions.getSupplierFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// GET Supplier DETAIL
export function * getSupplierDetailRequest (api) {
    let action = yield take(SupplierDetailTypes.GET_SUPPLIER_DETAIL_REQUEST)
    while (action !== END) {
        yield fork(getSupplierDetailRequestAPI, api, action)
        action = yield take(SupplierDetailTypes.GET_SUPPLIER_DETAIL_REQUEST)
    }
}

export function * getSupplierDetailRequestAPI (api, {data}) {
    try {
        const response = yield call(api.getSupplierDetail, data)
        if (response.ok) {
            return yield put(SupplierDetailActions.getSupplierDetailSuccess(response.data.data))
        } else {
            return yield put(SupplierDetailActions.getSupplierDetailFailure(response.data))
        }
    } catch (err) {
        return yield put(SupplierDetailActions.getSupplierDetailFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// ADD Supplier
export function * addSupplierRequest (api) {
    let action = yield take(SupplierTypes.ADD_SUPPLIER_REQUEST)
    while (action !== END) {
        yield fork(addSupplierRequestAPI, api, action)
        action = yield take(SupplierTypes.ADD_SUPPLIER_REQUEST)
    }
}

export function * addSupplierRequestAPI (api, {data}) {
    try {
        const response = yield call(api.addSupplier, data)
        if (response.ok) {
            const response = yield call(api.getSupplier, 1)
            return yield put(SupplierActions.addSupplierSuccess(response.data))
        } else {
            return yield put(SupplierActions.addSupplierFailure(response.data))
        }
    } catch (err) {
        return yield put(SupplierActions.addSupplierFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// DELETE Supplier
export function * deleteSupplierRequest (api) {
    let action = yield take(SupplierTypes.DELETE_SUPPLIER_REQUEST)
    while (action !== END) {
        yield fork(deleteSupplierRequestAPI, api, action)
        action = yield take(SupplierTypes.DELETE_SUPPLIER_REQUEST)
    }
}

export function * deleteSupplierRequestAPI (api, {data}) {
    try {
        const response = yield call(api.deleteSupplier, data)
        if (response.ok) {
            const response = yield call(api.getSupplier, 1)
            return yield put(SupplierActions.deleteSupplierSuccess(response.data))
        } else {
            return yield put(SupplierActions.deleteSupplierFailure(response.data))
        }
    } catch (err) {
        return yield put(SupplierActions.deleteSupplierFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// UPDATE Supplier
export function * updateSupplierRequest (api) {
    let action = yield take(SupplierTypes.UPDATE_SUPPLIER_REQUEST)
    while (action !== END) {
        yield fork(updateSupplierRequestAPI, api, action)
        action = yield take(SupplierTypes.UPDATE_SUPPLIER_REQUEST)
    }
}

export function * updateSupplierRequestAPI (api, {data, id}) {
    try {
        const response = yield call(api.updateSupplier, data, id)
        if (response.ok) {
            const response = yield call(api.getSupplier, 1)
            return yield put(SupplierActions.updateSupplierSuccess(response.data))
        } else {
            return yield put(SupplierActions.updateSupplierFailure(response.data))
        }
    } catch (err) {
        return yield put(SupplierActions.updateSupplierFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}