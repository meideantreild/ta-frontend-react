import { put, take, fork, call } from 'redux-saga/effects'
import { END } from 'redux-saga'

import SAWActions, { SAWTypes } from '../Redux/SAWRedux'
import SAWAddActions, { SAWAddTypes } from '../Redux/SAWAddRedux'

// GET SAW
export function * sawRequest (api) {
    let action = yield take(SAWTypes.SAW_REQUEST)
    while (action !== END) {
        yield fork(sawRequestAPI, api, action)
        action = yield take(SAWTypes.SAW_REQUEST)
    }
}

export function * sawRequestAPI (api, {data}) {
    try {
        const response = yield call(api.getSAW, data)
        if (response.ok) {
            return yield put(SAWActions.sawSuccess(response.data))
        } else {
            return yield put(SAWActions.sawFailure(response.data))
        }
    } catch (err) {
        return yield put(SAWActions.sawFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// ADD SAW
export function * sawAddRequest (api) {
    let action = yield take(SAWAddTypes.SAW_ADD_REQUEST)
    while (action !== END) {
        yield fork(sawAddRequestAPI, api, action)
        action = yield take(SAWAddTypes.SAW_ADD_REQUEST)
    }
}

export function * sawAddRequestAPI (api, {data}) {
    try {
        const response = yield call(api.addSAW, data)
        if (response.ok) {
            return yield put(SAWAddActions.sawAddSuccess(response.data))
        } else {
            return yield put(SAWAddActions.sawAddFailure(response.data))
        }
    } catch (err) {
        return yield put(SAWAddActions.sawAddFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}