import { put, take, fork, call } from 'redux-saga/effects'
import { END } from 'redux-saga'

import TopsisActions, { TopsisTypes } from '../Redux/TopsisRedux'
import TopsisAddActions, { TopsisAddTypes } from '../Redux/TopsisAddRedux'

// GET Topsis
export function * topsisRequest (api) {
    let action = yield take(TopsisTypes.TOPSIS_REQUEST)
    while (action !== END) {
        yield fork(topsisRequestAPI, api, action)
        action = yield take(TopsisTypes.TOPSIS_REQUEST)
    }
}

export function * topsisRequestAPI (api, {data}) {
    try {
        const response = yield call(api.getTopsis, data)
        if (response.ok) {
            return yield put(TopsisActions.topsisSuccess(response.data))
        } else {
            return yield put(TopsisActions.topsisFailure(response.data))
        }
    } catch (err) {
        return yield put(TopsisActions.topsisFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// ADD Topsis
export function * topsisAddRequest (api) {
    let action = yield take(TopsisAddTypes.TOPSIS_ADD_REQUEST)
    while (action !== END) {
        yield fork(topsisAddRequestAPI, api, action)
        action = yield take(TopsisAddTypes.TOPSIS_ADD_REQUEST)
    }
}

export function * topsisAddRequestAPI (api, {data}) {
    try {
        const response = yield call(api.addTopsis, data)
        if (response.ok) {
            return yield put(TopsisAddActions.topsisAddSuccess(response.data))
        } else {
            return yield put(TopsisAddActions.topsisAddFailure(response.data))
        }
    } catch (err) {
        return yield put(TopsisAddActions.topsisAddFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}