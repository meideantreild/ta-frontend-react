import { put, take, fork, call } from 'redux-saga/effects'
import { END } from 'redux-saga'

import StokKeluarActions, { StokKeluarTypes } from '../Redux/StokKeluarRedux'
import StokKeluarDetailActions, { StokKeluarDetailTypes } from '../Redux/StokKeluarDetailRedux'

// GET StokKeluar
export function * getStokKeluarRequest (api) {
    let action = yield take(StokKeluarTypes.GET_STOK_KELUAR_REQUEST)
    while (action !== END) {
        yield fork(getStokKeluarRequestAPI, api, action)
        action = yield take(StokKeluarTypes.GET_STOK_KELUAR_REQUEST)
    }
}

export function * getStokKeluarRequestAPI (api, {data}) {
    try {
        const response = yield call(api.getStokKeluar, data)
        if (response.ok) {
            return yield put(StokKeluarActions.getStokKeluarSuccess(response.data))
        } else {
            return yield put(StokKeluarActions.getStokKeluarFailure(response.data))
        }
    } catch (err) {
        return yield put(StokKeluarActions.getStokKeluarFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// GET StokKeluar DETAIL
export function * getStokKeluarDetailRequest (api) {
    let action = yield take(StokKeluarDetailTypes.GET_STOK_KELUAR_DETAIL_REQUEST)
    while (action !== END) {
        yield fork(getStokKeluarDetailRequestAPI, api, action)
        action = yield take(StokKeluarDetailTypes.GET_STOK_KELUAR_DETAIL_REQUEST)
    }
}

export function * getStokKeluarDetailRequestAPI (api, {data}) {
    try {
        const response = yield call(api.getStokKeluarDetail, data)
        console.log(response)
        if (response.ok) {
            return yield put(StokKeluarDetailActions.getStokKeluarDetailSuccess(response.data.data))
        } else {
            return yield put(StokKeluarDetailActions.getStokKeluarDetailFailure(response.data))
        }
    } catch (err) {
        return yield put(StokKeluarDetailActions.getStokKeluarDetailFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// ADD StokKeluar
export function * addStokKeluarRequest (api) {
    let action = yield take(StokKeluarTypes.ADD_STOK_KELUAR_REQUEST)
    while (action !== END) {
        yield fork(addStokKeluarRequestAPI, api, action)
        action = yield take(StokKeluarTypes.ADD_STOK_KELUAR_REQUEST)
    }
}

export function * addStokKeluarRequestAPI (api, {data}) {
    try {
        const response = yield call(api.addStokKeluar, data)
        if (response.ok) {
            const response = yield call(api.getStokKeluar, 1)
            return yield put(StokKeluarActions.addStokKeluarSuccess(response.data))
        } else {
            return yield put(StokKeluarActions.addStokKeluarFailure(response.data))
        }
    } catch (err) {
        return yield put(StokKeluarActions.addStokKeluarFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}

// DELETE StokKeluar
export function * deleteStokKeluarRequest (api) {
    let action = yield take(StokKeluarTypes.DELETE_STOK_KELUAR_REQUEST)
    while (action !== END) {
        yield fork(deleteStokKeluarRequestAPI, api, action)
        action = yield take(StokKeluarTypes.DELETE_STOK_KELUAR_REQUEST)
    }
}

export function * deleteStokKeluarRequestAPI (api, {data}) {
    try {
        const response = yield call(api.deleteStokKeluar, data)
        if (response.ok) {
            const response = yield call(api.getStokKeluar, 1)
            return yield put(StokKeluarActions.deleteStokKeluarSuccess(response.data))
        } else {
            return yield put(StokKeluarActions.deleteStokKeluarFailure(response.data))
        }
    } catch (err) {
        return yield put(StokKeluarActions.deleteStokKeluarFailure('Terjadi kesalahan, silahkan ulangi beberapa saat lagi'))
    }
}