import React, { Component } from 'react'
import { BrowserRouter, Switch } from 'react-router-dom'
import config from './config'

// Guard
import GuardRoute from './Containers/GuardRoute'

// Test
import Test from './Containers/Test'
import Login from './Containers/Login'
import BahanBaku from './Containers/BahanBaku'
import StokMasuk from './Containers/StokMasuk'
import StokKeluar from './Containers/StokKeluar'
import Produksi from './Containers/Produksi'
import Supplier from './Containers/Supplier'
import Penawaran from './Containers/Penawaran'
import Kriteria from './Containers/Kriteria'
import Topsis from './Containers/Topsis'
import SAW from './Containers/SAW'

export default class Entry extends Component {
    render () {
      return (
        <BrowserRouter basename={config.browserRouterBasename && config.browserRouterBasename !== '' ? config.browserRouterBasename : ''}>
          <Switch>
            <GuardRoute path='/login' component={Login} accesibleWhenLoggedIn={false}/>
            <GuardRoute path='/(|bahanbaku)' component={BahanBaku} accesibleWhenLoggedIn/>
            <GuardRoute path='/stokmasuk' component={StokMasuk} accesibleWhenLoggedIn/>
            <GuardRoute path='/stokkeluar' component={StokKeluar} accesibleWhenLoggedIn/>
            <GuardRoute path='/produksi' component={Produksi} accesibleWhenLoggedIn/>
            <GuardRoute path='/supplier' component={Supplier} accesibleWhenLoggedIn/>
            <GuardRoute path='/penawaran' component={Penawaran} accesibleWhenLoggedIn/>
            <GuardRoute path='/kriteria' component={Kriteria} accesibleWhenLoggedIn/>
            <GuardRoute path='/topsis' component={Topsis} accesibleWhenLoggedIn/>
            <GuardRoute path='/saw/:id' component={SAW} accesibleWhenLoggedIn/>
            <GuardRoute path='/saw' component={SAW} accesibleWhenLoggedIn/>
          </Switch>
        </BrowserRouter>
      )
    }
  }