import React, { Component } from 'react'
import _ from 'lodash'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { Layout, Menu, Button, message } from 'antd'
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    UserOutlined,
    VideoCameraOutlined,
    UploadOutlined,
} from '@ant-design/icons';
import Product from '../Statics/images/product.svg'
import Supply from '../Statics/images/supply.svg'
import Mathematics from '../Statics/images/mathematics.svg'
import Hiring from '../Statics/images/hiring.svg'
import Criteria from '../Statics/images/criteria.svg'

import Dispatcher from '../Containers/Dispatcher'

// Redux
import LogoutActions from '../ReduxSaga/Redux/LogoutRedux'

const { Header, Sider, Content } = Layout
const { SubMenu } = Menu

class LayoutMenu extends Component {
    constructor (props) {
      super(props)
      this.state = {
          logoutState: undefined,
          isLogout: false,
          collapsed: false,
          openSub: ''
      }
    }

    static getDerivedStateFromProps (nextProps, prevState) {
        let updated = {}
        if (!_.isEqual(nextProps.logoutState, prevState.logoutState) && !_.isEmpty(nextProps.logoutState)) {
            updated = { ...updated, logoutState: nextProps.logoutState }
        }
        if (!_.isEmpty(updated)) {
          return updated
        }
        return null
    }

    toggle = () => {
      this.setState({
        collapsed: !this.state.collapsed
      })
    }

    componentDidMount () {
        this.setState({
            openSub: [this.props.parent]
        })
    }

    changeSubMenu = (e) => {
        this.setState({
            openSub: e
        })
    }

    logout = () => {
        this.setState({
            isLogout: true
        })
        this.props.logoutRequest()
    }
    
    componentDidUpdate () {
        const { logoutState, isLogout } = this.state
        if ( isLogout ) {
            message.success(' Anda telah keluar, silahkan login untuk melanjutkan lagi')
            window.location.reload()
        }
    }

    render () {
        const { openSub } = this.state
        const { history, parent, type } = this.props
        console.log(this.state.isLogout)
        return (
            <Layout>
                <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
                <div className="logo" />
                <Menu theme="dark" mode="inline" defaultSelectedKeys={type} openKeys={openSub} onOpenChange={this.changeSubMenu}>
                    <SubMenu key="menu-bahanbaku" icon={<img src={Product} className='icon-menu'/>} title="Data Bahan Baku" >
                        <Menu.Item key='bahanbaku' onClick={() => this.props.history.push('/bahanbaku')}>Bahan Baku</Menu.Item>
                        <Menu.Item key='stokmasuk' onClick={() => this.props.history.push('/stokmasuk')}>Stok Masuk</Menu.Item>
                        <Menu.Item key='stokkeluar' onClick={() => this.props.history.push('/stokkeluar')}>Stok Keluar</Menu.Item>
                        <Menu.Item key='produksi' onClick={() => this.props.history.push('/produksi')}>Standar Produksi</Menu.Item>
                    </SubMenu>
                    <SubMenu key="menu-supplier" icon={<img src={Supply} className='icon-menu'/>} title="Data Supplier">
                        <Menu.Item key='supplier' onClick={() => this.props.history.push('/supplier')}>Supplier</Menu.Item>
                        <Menu.Item key='penawaran' onClick={() => this.props.history.push('/penawaran')}>Penawaran</Menu.Item>
                    </SubMenu>
                    <Menu.Item key="topsis" onClick={() => this.props.history.push('/topsis')} icon={<img src={Mathematics} className='icon-menu'/>}>
                        Pemilihan Bahan Baku
                    </Menu.Item>
                    <Menu.Item key="saw" onClick={() => this.props.history.push('/saw')} icon={<img src={Hiring} className='icon-menu'/>}>
                        Pemilihan Supplier
                    </Menu.Item>
                    <Menu.Item key="kriteria" onClick={() => this.props.history.push('/kriteria')} icon={<img src={Criteria} className='icon-menu'/>}>
                        Kriteria
                    </Menu.Item>
                </Menu>
                </Sider>
                <Layout className="site-layout">
                <Header className="site-layout-background">
                    <div className='plate-header'>
                        {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                        className: 'trigger',
                        onClick: this.toggle,
                        })}
                        <div className='title-plate-header'>
                            {
                                type === 'bahanbaku' ? (
                                    <span>Bahan Baku</span>
                                ) : null
                            }
                        </div>
                        <div className='button-plate-header'>
                            <Button type="primary" onClick={this.logout}>Keluar</Button>
                        </div>
                    </div>
                </Header>
                <Content
                    className="site-layout-background"
                    style={{
                    margin: '10px 16px',
                    minHeight: 280,
                    }}
                >
                    {this.props.children}
                </Content>
                </Layout>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => ({
    logoutState: state.logoutState,
})

const mapDispatchToProps = (dispatch) => ({
    logoutRequest: () => dispatch(LogoutActions.logoutRequest()),
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LayoutMenu))