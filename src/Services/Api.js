import config from '../config'

// a library to wrap and simplify api calls
import apisauce from 'apisauce'

const apiURL = config.apiURL

// our "constructor"
const create = (baseURL = apiURL) => {
  // ------
  // STEP 1
  // ------
  //
  // Create and configure an apisauce-based api object.
  //
  const apiWrapper = apisauce.create({
    // base URL is read from the "constructor"
    baseURL: apiURL,
    // 10 second timeout...
    timeout: 10000
  })

  const multipartForm = () => {
    return {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }
  }

  const multipartFormWithToken = (Authorization) => {
    return {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Authorization': `Bearer ${Authorization}`
      }
    }
  }

  const jsonHeader = () => {
    return {
      headers: {
        'Content-Type': 'application/json'
      }
    }
  }

  const headerWithToken = (Authorization) => {
    // JWT
    return {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Authorization}`
      }
    }
  }

  // Wrap api's addMonitor to allow the calling code to attach
  // additional monitors in the future.  But only in __DEV__ and only
  // if we've attached Reactotron to console (it isn't during unit tests).
  if (config.DEBUG) {
    const naviMonitor = (response) => console.log('API DEBUG! response =', response)
    apiWrapper.addMonitor(naviMonitor)
  }

  // ------
  // STEP 2
  // ------
  //
  // Define some functions that call the api.  The goal is to provide
  // a thin wrapper of the api layer providing nicer feeling functions
  // rather than "get", "post" and friends.
  //
  // I generally don't like wrapping the output at this level because
  // sometimes specific actions need to be take on `403` or `401`, etc.
  //
  // Since we can't hide from that, we embrace it by getting out of the
  // way at this level.
  //

  // Auth
  const login = (data) => {
    return apiWrapper.post(`/api/user/login`, data, jsonHeader())
  }

  // Bahan baku
  
  const getBahanBaku = (data) => {
    return apiWrapper.get(`/api/bahan-baku?page=${data}`, {}, jsonHeader())
  }
  
  const getBahanBakuDetail = (data) => {
    return apiWrapper.get(`/api/bahan-baku/${data}`, {}, jsonHeader())
  }
  
  const addBahanBaku = (data) => {
    return apiWrapper.post(`/api/bahan-baku`, data, jsonHeader())
  }
  
  const updateBahanBaku = (data, id) => {
    return apiWrapper.put(`/api/bahan-baku/${id}`, data, jsonHeader())
  }

  const deleteBahanBaku = (data) => {
    return apiWrapper.delete(`/api/bahan-baku/${data}`, {}, jsonHeader())
  }

  // Stok masuk
  
  const getStokMasuk = (data) => {
    return apiWrapper.get(`/api/stok-masuk?page=${data}`, {}, jsonHeader())
  }
  
  const getStokMasukDetail = (data) => {
    return apiWrapper.get(`/api/stok-masuk/${data}`, {}, jsonHeader())
  }
  
  const addStokMasuk = (data) => {
    return apiWrapper.post(`/api/stok-masuk`, data, jsonHeader())
  }

  const deleteStokMasuk = (data) => {
    return apiWrapper.delete(`/api/stok-masuk/${data}`, {}, jsonHeader())
  }

  // Stok keluar
  
  const getStokKeluar = (data) => {
    return apiWrapper.get(`/api/stok-keluar?page=${data}`, {}, jsonHeader())
  }
  
  const getStokKeluarDetail = (data) => {
    return apiWrapper.get(`/api/stok-keluar/${data}`, {}, jsonHeader())
  }
  
  const addStokKeluar = (data) => {
    return apiWrapper.post(`/api/stok-keluar`, data, jsonHeader())
  }

  const deleteStokKeluar = (data) => {
    return apiWrapper.delete(`/api/stok-keluar/${data}`, {}, jsonHeader())
  }

  //  Produksi
  
  const getProduksi = (data) => {
    return apiWrapper.get(`/api/produksi?page=${data}`, {}, jsonHeader())
  }
  
  const getProduksiDetail = (data) => {
    return apiWrapper.get(`/api/produksi/${data}`, {}, jsonHeader())
  }
  
  const updateProduksi = (data, id) => {
    return apiWrapper.put(`/api/produksi/${id}`, data, jsonHeader())
  }

  // Supplier
  
  const getSupplier = (data) => {
    return apiWrapper.get(`/api/supplier?page=${data}`, {}, jsonHeader())
  }
  
  const getSupplierDetail = (data) => {
    return apiWrapper.get(`/api/supplier/${data}`, {}, jsonHeader())
  }
  
  const addSupplier = (data) => {
    return apiWrapper.post(`/api/supplier`, data, jsonHeader())
  }
  
  const updateSupplier = (data, id) => {
    return apiWrapper.put(`/api/supplier/${id}`, data, jsonHeader())
  }

  const deleteSupplier = (data) => {
    return apiWrapper.put(`/api/supplier-delete/${data}`, {}, jsonHeader())
  }

  // Supplier
  
  const getPenawaran = (data) => {
    return apiWrapper.get(`/api/penawaran?page=${data}`, {}, jsonHeader())
  }
  
  const getPenawaranDetail = (data) => {
    return apiWrapper.get(`/api/penawaran/${data}`, {}, jsonHeader())
  }
  
  const addPenawaran = (data) => {
    return apiWrapper.post(`/api/penawaran`, data, jsonHeader())
  }
  
  const updatePenawaran = (data, id) => {
    return apiWrapper.put(`/api/penawaran/${id}`, data, jsonHeader())
  }

  const deletePenawaran = (data) => {
    return apiWrapper.delete(`/api/penawaran/${data}`, {}, jsonHeader())
  }

  //  Kriteria
  
  const getKriteria = (data) => {
    return apiWrapper.get(`/api/kriteria?page=${data}`, {}, jsonHeader())
  }
  
  const getKriteriaDetail = (data) => {
    return apiWrapper.get(`/api/kriteria/${data}`, {}, jsonHeader())
  }
  
  const updateKriteria = (data, id) => {
    return apiWrapper.put(`/api/kriteria/${id}`, data, jsonHeader())
  }

  // Topsis

  const getTopsis = (data) => {
    return apiWrapper.get(`/api/topsis?page=${data}`, {}, jsonHeader())
  }
  
  const addTopsis = () => {
    return apiWrapper.post(`/api/topsis`, {}, jsonHeader())
  }

  // SAW

  const getSAW = (data) => {
    return apiWrapper.get(`/api/saw?page=${data}`, {}, jsonHeader())
  }
  
  const addSAW = (data) => {
    return apiWrapper.post(`/api/saw/${data}`, {}, jsonHeader())
  }
  
  return {
    // a list of the API functions from step 2
    login,
    getBahanBaku,
    getBahanBakuDetail,
    addBahanBaku,
    updateBahanBaku,
    deleteBahanBaku,
    getStokMasuk,
    getStokMasukDetail,
    addStokMasuk,
    deleteStokMasuk,
    getStokKeluar,
    getStokKeluarDetail,
    addStokKeluar,
    deleteStokKeluar,
    getProduksi,
    getProduksiDetail,
    updateProduksi,
    getSupplier,
    getSupplierDetail,
    addSupplier,
    updateSupplier,
    deleteSupplier,
    getPenawaran,
    getPenawaranDetail,
    addPenawaran,
    updatePenawaran,
    deletePenawaran,
    getKriteria,
    getKriteriaDetail,
    updateKriteria,
    getTopsis,
    addTopsis,
    getSAW,
    addSAW
  }
}

// let's return back our create method as the default.
export default {
  create
}