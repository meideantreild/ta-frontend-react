import UpperCase from './UpperCase'
import NumberWithCommas from './NumberWithCommas'
import groupingCartItemsPerBrand from './GroupingCartItemsPerBrand'
import ReplaceFirstFiveChar from './ReplaceFirstFiveChar'
import ChangeStatusAndColor from './ChangeStatusAndColor'
import { getDeadline, SplitDateAndTime, getDateFormat } from './SplitDateAndTime'
import ChangeImageAndName from './ChangeImageAndName'
import InstructionImagePayment from './InstructionImagePayment'

export {
  UpperCase,
  NumberWithCommas,
  groupingCartItemsPerBrand,
  ReplaceFirstFiveChar,
  ChangeStatusAndColor,
  SplitDateAndTime,
  ChangeImageAndName,
  InstructionImagePayment,
  getDateFormat,
  getDeadline
}

export const BANK_LIST = [
  'BCA', 'MANDIRI', 'BNI', 'BRI',
  'CIMB', 'PERMATA', 'DANAMON', 'DKI'
]
