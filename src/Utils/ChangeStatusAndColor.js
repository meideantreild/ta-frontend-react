const ChangeStatusAndColor = (string) => {
  if (string === 'unpaid') return ['Menunggu Pembayaran', '#9b9b9b']
  else if (string !== 'unpaid') return ['Lihat Detail', '#4cab6b']
}

export const amountChangeColor = (string) => {
  const amountData = string
  const toStringAmount = amountData.toString() // Convert to string from int
  const amount = toStringAmount.includes('-') // Check contain
  if (amount === true) return ['#FF0000']
  else return ['#52c711']
}

export default ChangeStatusAndColor
