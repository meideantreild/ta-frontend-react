const ChangeImageAndName = (string) => {
  if (string === 'BMRI' || string === 'MANDIRI') return ['Virtual Account Bank Mandiri', 'https://upload.wikimedia.org/wikipedia/id/f/fa/Bank_Mandiri_logo.svg']
  else if (string === 'BNIN' || string === 'BNI') return ['Virtual Account BNI', 'https://upload.wikimedia.org/wikipedia/id/5/55/BNI_logo.svg']
  else if (string === 'BRIN' || string === 'BRI') return ['Virtual Account BRI', 'https://upload.wikimedia.org/wikipedia/commons/6/68/BANK_BRI_logo.svg']
  else if (string === 'BNIA' || string === 'CIMB') return ['Virtual Account CIMB Niaga', 'https://upload.wikimedia.org/wikipedia/commons/3/38/CIMB_Niaga_logo.svg']
  else if (string === 'BBBA' || string === 'PERMATA') return ['Virtual Account Permata', 'https://upload.wikimedia.org/wikipedia/id/4/48/PermataBank_logo.svg']
  else if (string === 'BDIN' || string === 'DANAMON') return ['Virtual Account Danamon', 'https://upload.wikimedia.org/wikipedia/id/7/7b/Danamon.svg']
  else if (string === 'CENA' || string === 'BCA') return ['Virtual Account BCA', 'https://upload.wikimedia.org/wikipedia/id/e/e0/BCA_logo.svg']
  else if (string === 'ALMA' || string === 'ALFAMART') return ['Pembayaran melalui Alfamart', 'https://upload.wikimedia.org/wikipedia/commons/8/86/Alfamart_logo.svg']
  else if (string === 'bank_transfer_manual') return ['Bank Transfer BNI', 'https://upload.wikimedia.org/wikipedia/id/5/55/BNI_logo.svg']
  else return ['empty', 'https://via.placeholder.com/40x40']
}
export default ChangeImageAndName
