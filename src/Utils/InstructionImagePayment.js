const InstructionImagePayment = (string) => {
  if (string === 'BMRI') return { 'ATM': '/payment/mandiri/VA_MANDIRI_ATM.png', 'Mobile Banking': '/payment/mandiri/VA_MANDIRI_MB.png', 'Internet Banking': '/payment/mandiri/VA_MANDIRI_IB.png' }
  else if (string === 'BNIN') return { 'ATM': '/payment/bni/VA_BNI_ATM.png', 'Mobile Banking': '/payment/bni/VA_BNI_MB.png', 'Internet Banking': '/payment/bni/VA_BNI_IB.png' }
  else if (string === 'BRIN') return { 'ATM': '/payment/bri/VA_BRI_ATM.png', 'Mobile Banking': '/payment/bri/VA_BRI_MB.png', 'Internet Banking': '/payment/bri/VA_BRI_IB.png' }
  else if (string === 'BNIA') return { 'ATM': '/payment/cimb/VA_CIMB_ATM.png', 'Mobile Banking': '/payment/cimb/VA_CIMB_MB.png', 'Internet Banking': '/payment/cimb/VA_CIMB_IB.png' }
  else if (string === 'BBBA') return { 'ATM': '/payment/permata/VA_PERMATA_ATM.png', 'Mobile Banking': '/payment/permata/VA_PERMATA_MB.png', 'Internet Banking': '/payment/permata/VA_PERMATA_IB.png' }
  else if (string === 'BDIN') return { 'ATM': '/payment/danamon/VA_DANAMON_ATM.png', 'Mobile Banking': '/payment/danamon/VA_DANAMON_MB.png' }
  else if (string === 'ALMA') return { 'Gerai Alfamart': '/payment/alfamart/ALFAMART_PAYMENT.png' }
  else if (string === 'bank_transfer_manual') return {}
  else return { 'Error': '/products/w_loadingImage.svg' }
}
export default InstructionImagePayment
