const groupingCartItemsPerBrand = (cartData) => {
  let cart = cartData
  let item = []
  let temp = {}
  let result = []
  if (cart && cart.items) {
    // These complex flow for extracting and grouping items for labeling purposes
    Object.keys(cart.items).forEach((key) => {
      Object.keys(cart.items[key]).forEach((k) => {
        item.push({ sender: key, item: cart.items[key][k] })
      })
    })

    item.forEach(x => {
      temp[x.sender] = temp[x.sender] || { 'sender': x.sender, 'items': [] }
      temp[x.sender].items = temp[x.sender].items.concat(x.item)
    })

    result = Object.keys(temp).map(k => temp[k])
    cart.items = result
  }

  return cart
}
export default groupingCartItemsPerBrand
