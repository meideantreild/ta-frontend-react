import React, { Component } from 'react'
import _ from 'lodash'
import { connect } from 'react-redux'
import { Table, Space, Button, Modal, Form, Row, Col, Input, InputNumber, message } from 'antd'

import Layout from '../Components/LayoutMenu'
import Loading from '../Components/Loading'
import { NumberWithCommas } from '../Utils'

// Redux
import ProduksiActions from '../ReduxSaga/Redux/ProduksiRedux'
import ProduksiDetailActions from '../ReduxSaga/Redux/ProduksiDetailRedux'

class Produksi extends Component {
    constructor (props) {
        super(props)
        this.state = {
            produksiState: undefined,
            produksiDetailState: undefined,
            page: 1,
            idDetail: null,
            modalEdit: false,
        }
        this.columns = [
            {
                title: 'Bahan Baku',
                dataIndex: 'nama_bahan_baku',
            },
            {
                title: 'Kepentingan',
                dataIndex: 'kepentingan_bahan_baku',
                align: 'center'
            },
            {
                title: 'Standar Produksi',
                dataIndex: 'standart_produksi',
                align: 'center'
            },
            {
                title: 'Aksi',
                align: 'center',
                render: (record) => (
                    <Space size="middle">
                        <a onClick={() => this.getDetail(record.id_produksi)}>Ubah Produksi</a>
                    </Space>
                ),
            },
        ]
    }

    static getDerivedStateFromProps (nextProps, prevState) {
        let updated = {}
        if (!_.isEqual(nextProps.produksiState, prevState.produksiState) && !_.isEmpty(nextProps.produksiState)) {
            updated = { ...updated, produksiState: nextProps.produksiState }
        }
        if (!_.isEqual(nextProps.produksiDetailState, prevState.produksiDetailState) && !_.isEmpty(nextProps.produksiDetailState)) {
            updated = { ...updated, produksiDetailState: nextProps.produksiDetailState }
        }
        if (!_.isEmpty(updated)) {
          return updated
        }
        return null
    }

    modalEdit = () => {
        this.setState(prevState => ({
          modalEdit: !prevState.modalEdit
        }))
    }

    getDetail = (e) => {
        this.setState({
            idDetail: e
        })
        this.props.getProduksiDetailRequest(e)
        this.modalEdit()
    }

    editData = values => {
        this.setState({
            page: 1
        })
        let payload = {
            kepentingan_bahan_baku: values.kepentingan_bahan_baku,
            standart_produksi: values.standart_produksi
        }
        message.info('Data berhasil diubah')
        this.props.updateProduksiRequest(payload, this.state.idDetail)
        this.modalEdit()
    }

    changePage = (e) => {
        this.props.getProduksiRequest(e)
        this.setState({
            page: e
        })
    }

    componentDidMount () {
      const { page } = this.state
      this.props.getProduksiRequest(page)
    }

    render () {
        const { produksiState, produksiDetailState, page } = this.state
        const data = !produksiState.fetching && produksiState.success && !_.isEmpty(produksiState.data) ? produksiState.data : {}
        const dataDetail = !produksiDetailState.fetching && produksiDetailState.success && !_.isEmpty(produksiDetailState.data) ? produksiDetailState.data : {}
        if (!produksiState.fetching && produksiState.success) {
            let sizePage = data.data.page * 10
            return (
                <Layout  parent='menu-bahanbaku' type='produksi'>
                    <Table
                        style={{marginTop: 10}}
                        columns={this.columns}
                        dataSource={data.data.data}
                        pagination={{
                            defaultCurrent: page,
                            total: sizePage,
                            showSizeChanger: false,
                            onChange: this.changePage
                        }}
                    />
                    {/* Edit Data */}
                    <Modal
                        title='Ubah Data Bahan Baku'
                        centered
                        visible={this.state.modalEdit}
                        onCancel={this.modalEdit}
                        footer={null}
                    >
                        {
                            !_.isEmpty(dataDetail) ? (
                                <Form
                                    layout='vertical'
                                    labelCol={{ span: 24 }}
                                    onFinish={this.editData}
                                >
                                    <Form.Item
                                        label='Bahan baku'
                                        name='nama_bahan_baku'
                                        initialValue={dataDetail.nama_bahan_baku}
                                    >
                                        <Input disabled/>
                                    </Form.Item>
                                    <Form.Item
                                        label='Kepentingan'
                                        name='kepentingan_bahan_baku'
                                        rules={
                                            [{
                                                required: true,
                                                message: 'Kepentingan bahan baku harus diisi!'
                                            },{
                                                type: 'number',
                                                min: 1,
                                                max: 5,
                                                message: 'Format kepentingan bahan baku adalah angka (min:1, max:5)!',
                                            }]
                                        }
                                        initialValue={ dataDetail.kepentingan_bahan_baku }
                                    >
                                        <InputNumber/>
                                    </Form.Item>
                                    <Form.Item
                                        label='Standar Produksi'
                                        name='standart_produksi'
                                        rules={
                                            [{
                                                required: true,
                                                message: 'Standar produksi harus diisi!'
                                            },{
                                                type: 'number',
                                                message: 'Format standar produksi adalah angka!'
                                            }]
                                        }
                                        initialValue={dataDetail.standart_produksi}
                                    >
                                        <InputNumber/>
                                    </Form.Item>
                                    <Form.Item
                                        style={{marginBottom: 0, textAlign: 'right'}}
                                    >
                                        <Button type="primary" htmlType="submit">
                                            Ubah Data
                                        </Button>
                                    </Form.Item>
                                </Form>
                            ) : null
                        }
                    </Modal>
                </Layout>
            )
        } else {
            return <Loading />    
        }
    }
}

const mapStateToProps = (state) => ({
    produksiState: state.produksiState,
    produksiDetailState: state.produksiDetailState
})

const mapDispatchToProps = (dispatch) => ({
    getProduksiRequest: (data) => dispatch(ProduksiActions.getProduksiRequest(data)),
    getProduksiDetailRequest: (data) => dispatch(ProduksiDetailActions.getProduksiDetailRequest(data)),
    updateProduksiRequest: (data, id) => dispatch(ProduksiActions.updateProduksiRequest(data, id))
})

export default connect(mapStateToProps, mapDispatchToProps)(Produksi)