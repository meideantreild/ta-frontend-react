import React, { Component } from 'react'
import _ from 'lodash'
import { connect } from 'react-redux'
import moment from 'moment'
import { Table, Space, Button, Modal, Form, Row, Col, Input, Select, Cascader, InputNumber, DatePicker, message } from 'antd'

import Layout from '../Components/LayoutMenu'
import Loading from '../Components/Loading'
import { NumberWithCommas } from '../Utils'

// Redux
import SAWActions from '../ReduxSaga/Redux/SAWRedux'
import SAWAddActions from '../ReduxSaga/Redux/SAWAddRedux'

const dateFormat = 'YYYY-MM-DD'
class SAW extends Component {
    constructor (props) {
        super(props)
        this.state = {
            sawState: undefined,
            sawAddState: undefined,
            countData: null,
            page: 1,
            id: 0,
            modalAdd: false,
            addingData: false,
            modalAfterAdd: false,
        }
        this.columns = [
            {
                title: 'Tanggal Pemilihan',
                dataIndex: 'tanggal_saw',
                align: 'center'
            },
            {
                title: 'Nama Supplier',
                dataIndex: 'nama_supplier',
            },
            {
                title: 'Bahan Baku',
                dataIndex: 'nama_bahan_baku',
            },
            {
                title: 'Harga',
                dataIndex: 'harga_saw',
                render: (value) => (
                    <Space size="middle">
                        Rp {NumberWithCommas(value)}
                    </Space>
                )
            },
            {
                title: 'Ongkos',
                dataIndex: 'ongkos_saw',
                render: (value) => (
                    <Space size="middle">
                        Rp {NumberWithCommas(value)}
                    </Space>
                )
            },
            {
                title: 'Stok',
                dataIndex: 'stok_saw',
                align: 'center'
            },
            {
                title: 'Kualitas',
                dataIndex: 'kualitas_saw',
                align: 'center'
            },
            {
                title: 'Nilai Prefensi',
                dataIndex: 'nilai_prefensi',
                align: 'center'
            }
        ]
        // Kriteria
        this.columnsKriteria = [
            {
                title: 'Nama Kriteria',
                dataIndex: 'nama_kriteria',
            },
            {
                title: 'Bobot Kriteria',
                dataIndex: 'bobot_kriteria',
                align: 'center'
            },
            {
                title: 'Jenis Kriteria',
                dataIndex: 'jenis_kriteria',
                render: (value) => (
                    value === 1 ? (
                        <Space size="middle">
                            Cost / Biaya
                        </Space>
                    ) : value === 2 ? (
                        <Space size="middle">
                            Benefit / Keuntungan
                        </Space>
                    ) : null
                )
            },
            {
                title: 'Deskripsi Kriteria',
                dataIndex: 'deskripsi_kriteria'
            }
        ]
        // Bahan Baku
        this.columnsDataPenawaran = [
            {
                title: 'Nama Supplier',
                dataIndex: 'nama_supplier'
            },
            {
                title: 'Nama Bahan Baku',
                dataIndex: 'nama_bahan_baku'
            },
            {
                title: 'Harga',
                dataIndex: 'harga_penawaran',
                render: (value) => (
                    <Space size="middle">
                        Rp {NumberWithCommas(value)}
                    </Space>
                )
            },
            {
                title: 'Ongkos',
                dataIndex: 'ongkos_penawaran',
                render: (value) => (
                    <Space size="middle">
                        Rp {NumberWithCommas(value)}
                    </Space>
                )
            },
            {
                title: 'Kualitas',
                dataIndex: 'kualitas_penawaran',
                align: 'center'
            },
            {
                title: 'Stok',
                dataIndex: 'stok_penawaran',
                align: 'center'
            },
        ]
        // Normalisasi
        this.columnsDataNormalisasi = [
            {
                title: 'Nama Supplier',
                dataIndex: 'nama_supplier'
            },
            {
                title: 'Nama Bahan Baku',
                dataIndex: 'nama_bahan_baku'
            },
            {
                title: 'Nilai Harga Normalisasi',
                dataIndex: 'harga_normalisasi',
                align: 'center'
            },
            {
                title: 'Nilai Ongkos Normalisasi',
                dataIndex: 'ongkos_normalisasi',
                align: 'center'
            },
            {
                title: 'Nilai Kualitas Normalisasi',
                dataIndex: 'kualitas_normalisasi',
                align: 'center'
            },
            {
                title: 'Nilai Stok Normalisasi',
                dataIndex: 'stok_normalisasi',
                align: 'center'
            }
        ]
        // Normalisasi Terbobot
        this.columnsDataRanking = [
            {
                title: 'Nama Supplier',
                dataIndex: 'nama_supplier'
            },
            {
                title: 'Nama Bahan Baku',
                dataIndex: 'nama_bahan_baku'
            },
            {
                title: 'Nilai Harga Ranking',
                dataIndex: 'harga_ranking',
                align: 'center'
            },
            {
                title: 'Nilai Ongkos Ranking',
                dataIndex: 'ongkos_ranking',
                align: 'center'
            },
            {
                title: 'Nilai Kualitas Ranking',
                dataIndex: 'kualitas_ranking',
                align: 'center'
            },
            {
                title: 'Nilai Stok Ranking',
                dataIndex: 'stok_ranking',
                align: 'center'
            },
            {
                title: 'Total Ranking',
                dataIndex: 'total_ranking',
                align: 'center'
            }
        ]
        // Hasil Prefensi
        this.columnsHasilPrefensi = [
            {
                title: 'Nama Supplier',
                dataIndex: 'nama_supplier'
            },
            {
                title: 'Nama Bahan Baku',
                dataIndex: 'nama_bahan_baku'
            },
            {
                title: 'Nilai Prefensi',
                dataIndex: 'hasil_ranking',
                align: 'center'
            }
        ]
    }

    static getDerivedStateFromProps (nextProps, prevState) {
        let updated = {}
        if (!_.isEqual(nextProps.sawState, prevState.sawState) && !_.isEmpty(nextProps.sawState)) {
            updated = { ...updated, sawState: nextProps.sawState}
        }
        if (!_.isEqual(nextProps.sawAddState, prevState.sawAddState) && !_.isEmpty(nextProps.sawAddState)) {
            updated = { ...updated, sawAddState: nextProps.sawAddState}
        }
        if (!_.isEmpty(updated)) {
          return updated
        }
        return null
    }

    handleSelectBahanBaku = value => {
        this.setState({
            id: value
        })
    }

    modalAdd = () => {
        this.setState(prevState => ({
          modalAdd: !prevState.modalAdd
        }))
    }

    addData = () => {
        this.setState({
            addingData: true
        })
        this.props.sawAddRequest(this.state.id)
        this.modalAdd()
    }

    modalAfterAdd = () => {
        this.setState(prevState => ({
          modalAfterAdd: !prevState.modalAfterAdd
        }))
    }

    modalCloseAfterAdd = () => {
        this.setState(prevState => ({
          modalAfterAdd: !prevState.modalAfterAdd
        }))
        this.props.sawRequest(1)
    }

    changePage = (e) => {
        this.props.sawRequest(e)
        this.setState({
            page: e
        })
    }

    componentDidMount () {
      const { page } = this.state
      if (!_.isEmpty(this.props.match.params)) {
        this.setState({
            id: this.props.match.params.id,
            addingData: true
        })
        this.props.sawAddRequest(this.props.match.params.id)
      }
      this.props.sawRequest(page)
    }

    componentDidUpdate () {
        const { sawAddState, addingData } = this.state
        if (!sawAddState.fetching && sawAddState.success && !_.isEmpty(sawAddState.data) && addingData) {
            this.setState({
                modalAfterAdd: true,
                addingData: false
            })
        }
    }

    render () {
        const { sawState, sawAddState, page, countData } = this.state
        let dataBahan = []
        const data = !sawState.fetching && sawState.success && !_.isEmpty(sawState.data) ? sawState.data : {}
        if (!_.isEmpty(data)) {
            data.data.bahan_baku.map((arrayData) =>
                dataBahan.push({
                    value: arrayData.id_bahan_baku,
                    label: arrayData.nama_bahan_baku
                })
            )
        }
        if (!sawState.fetching && sawState.success) {
            let sizePage = data.data.page * 10
            return (
                <Layout  parent='menu-saw' type='saw'>
                    <div style={{textAlign: 'right'}}>
                        <Button type="secondary" onClick={this.modalAdd}>Tambah Data</Button>
                    </div>
                    <Table
                        style={{marginTop: 10}}
                        columns={this.columns}
                        dataSource={data.data.data}
                        pagination={{
                            defaultCurrent: page,
                            total: sizePage,
                            showSizeChanger: false,
                            onChange: this.changePage,
                        }}
                    />
                    {/* Tambah Data */}
                    <Modal
                        title='Memilih Supplier'
                        centered
                        visible={this.state.modalAdd}
                        onCancel={this.modalAdd}
                        onOk={this.addData}
                    >
                        <Form
                            layout='vertical'
                            labelCol={{ span: 24 }}
                        >
                            <Col className='gutter-row' span={24}>
                                <Form.Item
                                    label='Pilih Bahan baku'
                                    name='bahan_baku'
                                    rules={[{
                                        required: true,
                                        message: 'Nama bahan baku harus dipilih!'
                                    }]}
                                >
                                    <Cascader options={dataBahan} onChange={this.handleSelectBahanBaku}/>
                                </Form.Item>
                            </Col>
                        </Form>
                    </Modal>
                    {/* Tampil Data */}
                    {
                        !sawAddState.fetching && sawAddState.success && sawAddState.data.data.jumlahData !== 0 ? (
                            <Modal
                                title='Hasil Perhitungan'
                                centered
                                width={`auto`}
                                visible={this.state.modalAfterAdd}
                                onCancel={this.modalCloseAfterAdd}
                                onOk={this.modalCloseAfterAdd}
                                width={1280}
                            >
                                <div className='text-bolder'>Data Kriteria</div>
                                <Table
                                    style={{margin: '10px 0px'}}
                                    columns={this.columnsKriteria}
                                    dataSource={sawAddState.data.data.dataSAW.dataKriteria}
                                    pagination={{
                                        disabled: true,
                                        hideOnSinglePage: true
                                    }}
                                />
                                <div className='text-bolder'>Data Penawaran</div>
                                <Table
                                    style={{margin: '10px 0px'}}
                                    columns={this.columnsDataPenawaran}
                                    dataSource={sawAddState.data.data.dataSAW.dataPenawaran}
                                    pagination={{
                                        disabled: true,
                                        hideOnSinglePage: true
                                    }}
                                />
                                <div className='text-bolder'>Data Normalisasi</div>
                                <Table
                                    style={{margin: '10px 0px'}}
                                    columns={this.columnsDataNormalisasi}
                                    dataSource={sawAddState.data.data.dataNormalisasi}
                                    pagination={{
                                        disabled: true,
                                        hideOnSinglePage: true
                                    }}
                                />
                                <div className='text-bolder'>Data Ranking</div>
                                <Table
                                    style={{margin: '10px 0px'}}
                                    columns={this.columnsDataRanking}
                                    dataSource={sawAddState.data.data.totalRanking}
                                    pagination={{
                                        disabled: true,
                                        hideOnSinglePage: true
                                    }}
                                />
                                <div className='text-bolder'>Hasil Prefensi</div>
                                <Table
                                    style={{margin: '10px 0px'}}
                                    columns={this.columnsHasilPrefensi}
                                    dataSource={sawAddState.data.data.rankingPilihan}
                                    pagination={{
                                        disabled: true,
                                        hideOnSinglePage: true
                                    }}
                                />
                            </Modal>
                        ) : !sawAddState.fetching && sawAddState.success && sawAddState.data.data.jumlahData === 0 ? (
                            <Modal
                                title='Data Kosong'
                                centered
                                width={`auto`}
                                visible={this.state.modalAfterAdd}
                                onCancel={this.modalCloseAfterAdd}
                                onOk={() => this.props.history.push('/penawaran')}
                                okText='Halaman Penawaran'
                                width={720}
                            >
                                <div>Data penawaran bahan baku {sawAddState.message} masih kosong, silahkan tambah data penawaran terlebih dahulu</div>
                            </Modal>
                        ) : null
                    }
                </Layout>
            )
        } else {
            return <Loading />
        }
    }
}

const mapStateToProps = (state) => ({
    sawState: state.sawState,
    sawAddState: state.sawAddState
})

const mapDispatchToProps = (dispatch) => ({
    sawRequest: (data) => dispatch(SAWActions.sawRequest(data)),
    sawAddRequest: (data) => dispatch(SAWAddActions.sawAddRequest(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(SAW)