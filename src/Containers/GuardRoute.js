import React, { Component } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import { Route, Redirect } from 'react-router-dom'

// Loading Component
import Loading from '../Components/Loading'

// Redux
import RouteActions from '../ReduxSaga/Redux/RouteRedux'

class GuardRoute extends Component {
  constructor (props) {
    super(props)
    this.state = {
      routeState: undefined,
      accesibleWhenLoggedIn: false,
      animated: false
    }
  }

  static getDerivedStateFromProps (nextProps, prevState) {
    let changes = {}
    if (!_.isEqual(prevState.routeState, nextProps.routeState)) {
      changes = { ...changes, routeState: nextProps.routeState }
    }
    changes = { ...changes, accesibleWhenLoggedIn: nextProps.accesibleWhenLoggedIn, animated: nextProps.animated }
    return changes
  }

  // keepOnPage(e) {
  //   var message = 'Warning!\n\nNavigating away from this page will delete your text if you haven\'t already saved it.';
  //   e.returnValue = message;
  //   return message;
  // }

  componentDidMount () {
    this.props.authenticateUser()
    // window.addEventListener('beforeunload', this.keepOnPage)
  }

  componentWillUnmount() {
    // window.removeEventListener('beforeunload', this.keepOnPage);
  }

  render () {
    const { routeState, accesibleWhenLoggedIn } = this.state
    const { component: Component, ...rest } = this.props
    if (_.isEmpty(routeState.data) || routeState.fetching) {
      return <Loading />
    } else {
      if (!_.isEmpty(routeState.data) && routeState.data.hasOwnProperty('token')) {
        const authToken = routeState.data.token
        return (
          <Route
            {...rest}
            render={props => {
              if (authToken === 'NO_TOKEN') {
                if (props.location.pathname !== '/login') {
                  if (!accesibleWhenLoggedIn) {
                    return <Component {...props} />
                  } else {
                    return (
                      <Redirect
                        to={{
                          pathname: '/login',
                          state: { from: props.location.pathname }
                        }}
                      />
                    )
                  }
                } else {
                  return <Component {...props} />
                }
              } else {
                if (accesibleWhenLoggedIn) {
                  return <Component {...props} />
                } else {
                  return (
                    <Redirect
                      to={{
                        pathname: '/',
                        state: { from: props.location.pathname }
                      }}
                    />
                  )
                }
              }
            }}
          />
        )
      }
    }
  }
}

const mapStateFromProps = state => ({
  routeState: state.routeState
})

const mapDispatchFromProps = dispatch => ({
  authenticateUser: () => dispatch(RouteActions.routeRequest())
})

export default connect(mapStateFromProps,mapDispatchFromProps)(GuardRoute)