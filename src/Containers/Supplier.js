import React, { Component } from 'react'
import _ from 'lodash'
import { connect } from 'react-redux'
import { Table, Space, Button, Modal, Form, Row, Col, Input, InputNumber, message } from 'antd'

import Layout from '../Components/LayoutMenu'
import Loading from '../Components/Loading'
import { NumberWithCommas } from '../Utils'

// Redux
import SupplierActions from '../ReduxSaga/Redux/SupplierRedux'
import SupplierDetailActions from '../ReduxSaga/Redux/SupplierDetailRedux'

class Supplier extends Component {
    constructor (props) {
        super(props)
        this.state = {
            supplierState: undefined,
            supplierDetailState: undefined,
            countData: null,
            page: 1,
            idDetail: null,
            modalAdd: false,
            modalEdit: false,
            modalDelete: false,
        }
        this.columns = [
            {
                title: 'Nama Supplier',
                dataIndex: 'nama_supplier',
            },
            {
                title: 'Nomor Telepon',
                dataIndex: 'no_telp_supplier',
            },
            {
                title: 'Alamat Supplier',
                dataIndex: 'alamat_supplier',
            },
            {
                title: 'Aksi',
                align: 'center',
                render: (record) => (
                    <Space size="middle">
                        <a onClick={() => this.getDetail(record.id_supplier)}>Ubah Data</a>
                        <a onClick={() => this.getDeleteDetail(record.id_supplier)}> Hapus Data</a>
                    </Space>
                ),
            },
        ]
    }

    static getDerivedStateFromProps (nextProps, prevState) {
        let updated = {}
        if (!_.isEqual(nextProps.supplierState, prevState.supplierState) && !_.isEmpty(nextProps.supplierState)) {
            updated = { ...updated, supplierState: nextProps.supplierState }
        }
        if (!_.isEqual(nextProps.supplierDetailState, prevState.supplierDetailState) && !_.isEmpty(nextProps.supplierDetailState)) {
            updated = { ...updated, supplierDetailState: nextProps.supplierDetailState }
        }
        if (!_.isEmpty(updated)) {
          return updated
        }
        return null
    }

    modalAdd = () => {
        this.setState(prevState => ({
          modalAdd: !prevState.modalAdd
        }))
    }

    modalEdit = () => {
        this.setState(prevState => ({
          modalEdit: !prevState.modalEdit
        }))
    }

    modalDelete = () => {
        this.setState(prevState => ({
          modalDelete: !prevState.modalDelete
        }))
    }

    getDetail = (e) => {
        this.setState({
            idDetail: e
        })
        this.props.getSupplierDetailRequest(e)
        this.modalEdit()
    }

    getDeleteDetail = (e) => {
        this.setState({
            idDetail: e
        })
        this.props.getSupplierDetailRequest(e)
        this.modalDelete()
    }

    addData = values => {
        this.setState({
            page: 1
        })
        const payload = {
            alamat_supplier: values.alamat_supplier,
            nama_supplier: values.nama_supplier,
            no_telp_supplier: values.no_telp_supplier,
            status_supplier: 1
        }
        this.props.addSupplierRequest(payload)
        this.modalAdd()
    }

    editData = values => {
        this.setState({
            page: 1
        })
        message.info('Data berhasil diubah')
        this.props.updateSupplierRequest(values, this.state.idDetail)
        this.modalEdit()
    }

    deleteData = () => {
        this.setState({
            page: 1,
            countData: this.state.supplierState.data.data.data.length
        })
        this.props.deleteSupplierRequest(this.state.idDetail)
        this.modalDelete()
    }  

    changePage = (e) => {
        this.props.getSupplierRequest(e)
        this.setState({
            page: e
        })
    }

    componentDidMount () {
      const { page } = this.state
      this.props.getSupplierRequest(page)
    }

    render () {
        const { supplierState, supplierDetailState, page, countData } = this.state
        const data = !supplierState.fetching && supplierState.success && !_.isEmpty(supplierState.data) ? supplierState.data : {}
        const dataDetail = !supplierDetailState.fetching && supplierDetailState.success && !_.isEmpty(supplierDetailState.data) ? supplierDetailState.data : {}
        if (!supplierState.fetching && supplierState.success) {
            if (countData !== null) {
                if (countData === supplierState.data.data.data.length) {
                    message.info('Data gagal dihapus!')
                } else {
                    message.info('Data berhasil dihapus')
                }
                this.setState({
                    countData: null
                })
            }
            let sizePage = data.data.page * 10
            return (
                <Layout  parent='menu-supplier' type='supplier'>
                    <div style={{textAlign: 'right'}}>
                        <Button type="secondary" onClick={this.modalAdd}>Tambah Data</Button>
                    </div>
                    <Table
                        style={{marginTop: 10}}
                        columns={this.columns}
                        dataSource={data.data.data}
                        pagination={{
                            defaultCurrent: page,
                            total: sizePage,
                            showSizeChanger: false,
                            onChange: this.changePage
                        }}
                    />
                    {/* Tambah Data */}
                    <Modal
                        title='Tambah Data Bahan Baku'
                        centered
                        visible={this.state.modalAdd}
                        onCancel={this.modalAdd}
                        footer={null}
                        width={720}
                    >
                        <Form
                            layout='vertical'
                            labelCol={{ span: 24 }}
                            onFinish={this.addData}
                        >
                        <Row gutter={12}>
                            <Col className='gutter-row' span={12}>
                                <Form.Item
                                    label='Nama Supplier'
                                    name='nama_supplier'
                                    rules={[{
                                        required: true,
                                        message: 'Nama supplier harus diisi!'
                                    }]}
                                >
                                    <Input />
                                </Form.Item>
                            </Col>
                            <Col className='gutter-row' span={12}>
                                <Form.Item
                                    label='Alamat Supplier'
                                    name='alamat_supplier'
                                    rules={[{
                                        required: true,
                                        message: 'Alamat supplier harus diisi!'
                                    }]}
                                >
                                    <Input />
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={12}>
                            <Col className='gutter-row' span={12}>
                                <Form.Item
                                    label='Nomor Telepon'
                                    name='no_telp_supplier'
                                    rules={
                                        [{
                                            required: true,
                                            message: 'Nomor Telepon harus diisi!'
                                        },{
                                            type: 'number',
                                            message: 'Format nomor telepon adalah angka!'
                                        }]
                                    }
                                >
                                    <InputNumber/>
                                </Form.Item>
                            </Col>
                        </Row>
                            <Form.Item
                                style={{marginBottom: 0, textAlign: 'right'}}
                            >
                                <Button type="primary" htmlType="submit">
                                    Simpan Data
                                </Button>
                            </Form.Item>
                        </Form>
                    </Modal>
                    {/* Edit Data */}
                    <Modal
                        title='Ubah Data Bahan Baku'
                        centered
                        visible={this.state.modalEdit}
                        onCancel={this.modalEdit}
                        footer={null}
                        width={720}
                    >
                        {
                            !_.isEmpty(dataDetail) ? (
                                <Form
                                    layout='vertical'
                                    labelCol={{ span: 24 }}
                                    onFinish={this.editData}
                                >
                                    <Row gutter={12}>
                                        <Col className='gutter-row' span={12}>
                                            <Form.Item
                                                label='Nama Supplier'
                                                name='nama_supplier'
                                                rules={[{
                                                    required: true,
                                                    message: 'Nama supplier harus diisi!'
                                                }]}
                                                initialValue={dataDetail.nama_supplier}
                                            >
                                                <Input/>
                                            </Form.Item>
                                        </Col>
                                        <Col className='gutter-row' span={12}>
                                            <Form.Item
                                                label='Alamat Supplier'
                                                name='alamat_supplier'
                                                rules={[{
                                                        required: true,
                                                        message: 'Alamat supplier harus diisi!'
                                                    }]}
                                                initialValue={dataDetail.alamat_supplier}
                                            >
                                                <Input/>
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                    <Row gutter={12}>
                                        <Col className='gutter-row' span={12}>
                                            <Form.Item
                                                label='Nomor Telepon'
                                                name='no_telp_supplier'
                                                rules={
                                                    [{
                                                        required: true,
                                                        message: 'Nomor telepon harus diisi!'
                                                    },{
                                                        type: 'number',
                                                        message: 'Format nomor telepon adalah angka!'
                                                    }]
                                                }
                                                initialValue={dataDetail.no_telp_supplier}
                                            >
                                                <InputNumber/>
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                    <Form.Item
                                        style={{marginBottom: 0, textAlign: 'right'}}
                                    >
                                        <Button type="primary" htmlType="submit">
                                            Ubah Data
                                        </Button>
                                    </Form.Item>
                                </Form>
                            ) : null
                        }
                    </Modal>
                    {/* Delete Data */}
                    <Modal
                        title='Ubah Data Bahan Baku'
                        centered
                        visible={this.state.modalDelete}
                        onCancel={this.modalDelete}
                        onOk={this.deleteData}
                    >
                        {
                            !_.isEmpty(dataDetail) ? (
                                <div>Apakah anda yakin ingin menghapus data supplier {dataDetail.nama_supplier}?</div>
                            ) : <div>Memuat data...</div>
                        }   
                    </Modal>
                </Layout>
            )
        } else {
            return <Loading />    
        }
    }
}

const mapStateToProps = (state) => ({
    supplierState: state.supplierState,
    supplierDetailState: state.supplierDetailState
})

const mapDispatchToProps = (dispatch) => ({
    getSupplierRequest: (data) => dispatch(SupplierActions.getSupplierRequest(data)),
    getSupplierDetailRequest: (data) => dispatch(SupplierDetailActions.getSupplierDetailRequest(data)),
    addSupplierRequest: (data) => dispatch(SupplierActions.addSupplierRequest(data)),
    updateSupplierRequest: (data, id) => dispatch(SupplierActions.updateSupplierRequest(data, id)),
    deleteSupplierRequest: (data) => dispatch(SupplierActions.deleteSupplierRequest(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(Supplier)