import React, { Component } from 'react'
import _ from 'lodash'
import { connect } from 'react-redux'
import moment from 'moment'
import { Table, Space, Button, Modal, Form, Row, Col, Input, Select, Cascader, InputNumber, DatePicker, message } from 'antd'

import Layout from '../Components/LayoutMenu'
import Loading from '../Components/Loading'
import { NumberWithCommas } from '../Utils'

// Redux
import TopsisActions from '../ReduxSaga/Redux/TopsisRedux'
import TopsisAddActions from '../ReduxSaga/Redux/TopsisAddRedux'

const dateFormat = 'YYYY-MM-DD'
class Topsis extends Component {
    constructor (props) {
        super(props)
        this.state = {
            topsisState: undefined,
            topsisAddState: undefined,
            countData: null,
            page: 1,
            modalAdd: false,
            addingData: false,
            modalAfterAdd: false,
        }
        this.columns = [
            {
                title: 'Tanggal Pemilihan',
                dataIndex: 'tanggal_topsis',
                align: 'center'
            },
            {
                title: 'Bahan Baku',
                dataIndex: 'nama_bahan_baku',
            },
            {
                title: 'Harga Beli',
                dataIndex: 'harga_topsis',
                render: (value) => (
                    <Space size="middle">
                        Rp {NumberWithCommas(value)}
                    </Space>
                )
            },
            {
                title: 'Stok',
                dataIndex: 'stok_topsis',
                align: 'center'
            },
            {
                title: 'Kebutuhan',
                dataIndex: 'kebutuhan_topsis',
                align: 'center'
            },
            {
                title: 'Ketersediaan',
                dataIndex: 'ketersediaan_topsis',
                align: 'center'
            },
            {
                title: 'Nilai Prefensi',
                dataIndex: 'nilai_prefensi',
                align: 'center'
            }
        ]
        // Kriteria
        this.columnsKriteria = [
            {
                title: 'Nama Kriteria',
                dataIndex: 'nama_kriteria',
            },
            {
                title: 'Bobot Kriteria',
                dataIndex: 'bobot_kriteria',
                align: 'center'
            },
            {
                title: 'Jenis Kriteria',
                dataIndex: 'jenis_kriteria',
                render: (value) => (
                    value === 1 ? (
                        <Space size="middle">
                            Cost / Biaya
                        </Space>
                    ) : value === 2 ? (
                        <Space size="middle">
                            Benefit / Keuntungan
                        </Space>
                    ) : null
                )
            },
            {
                title: 'Deskripsi Kriteria',
                dataIndex: 'deskripsi_kriteria'
            }
        ]
        // Bahan Baku
        this.columnsDataBarang = [
            {
                title: 'Nama Bahan Baku',
                dataIndex: 'nama_bahan_baku'
            },
            {
                title: 'Kepentingan',
                dataIndex: 'kepentingan_bahan_baku',
                align: 'center'
            },
            {
                title: 'Ketersediaan',
                dataIndex: 'ketersediaan',
                align: 'center'
            },
            {
                title: 'Harga Terakhir',
                dataIndex: 'harga_terakhir',
                render: (value) => (
                    <Space size="middle">
                        Rp {NumberWithCommas(value)}
                    </Space>
                )
            },
            {
                title: 'Harga Kriteria',
                dataIndex: 'kriteria_harga',
                render: (value) => (
                    <Space size="middle">
                        Rp {NumberWithCommas(value)}
                    </Space>
                )
            },
            {
                title: 'Stok',
                dataIndex: 'stok_bahan_baku',
                align: 'center'
            },
            {
                title: 'Presentase Stok',
                dataIndex: 'presentase_stok',
                align: 'center'
            }
        ]
        // Normalisasi
        this.columnsDataNormalisasi = [
            {
                title: 'Nama Bahan Baku',
                dataIndex: 'nama_bahan_baku'
            },
            {
                title: 'Nilai Harga Normalisasi',
                dataIndex: 'harga_normalisasi',
                align: 'center'
            },
            {
                title: 'Nilai Stok Normalisasi',
                dataIndex: 'stok_normalisasi',
                align: 'center'
            },
            {
                title: 'Nilai Kepentingan Normalisasi',
                dataIndex: 'kepentingan_normalisasi',
                align: 'center'
            },
            {
                title: 'Nilai Ketersediaan Normalisasi',
                dataIndex: 'ketersediaan_normalisasi',
                align: 'center'
            }
        ]
        // Normalisasi Terbobot
        this.columnsDataNormalisasiTerbobot = [
            {
                title: 'Nama Bahan Baku',
                dataIndex: 'nama_bahan_baku'
            },
            {
                title: 'Nilai Harga Normalisasi Terbobot',
                dataIndex: 'harga_terbobot',
                align: 'center'
            },
            {
                title: 'Nilai Stok Normalisasi Terbobot',
                dataIndex: 'stok_terbobot',
                align: 'center'
            },
            {
                title: 'Nilai Kepentingan Normalisasi Terbobot',
                dataIndex: 'kepentingan_terbobot',
                align: 'center'
            },
            {
                title: 'Nilai Ketersediaan Normalisasi Terbobot',
                dataIndex: 'ketersediaan_terbobot',
                align: 'center'
            }
        ]
        // Ideal Positif
        this.columnsDataIdealPositif = [
            {
                title: 'Solusi Ideal',
                render: () => (
                    <Space size="middle">
                        Ideal Positif
                    </Space>
                )
            },
            {
                title: 'Nilai Ideal Harga',
                dataIndex: 'harga_positif',
                align: 'center'
            },
            {
                title: 'Nilai Ideal Stok',
                dataIndex: 'stok_positif',
                align: 'center'
            },
            {
                title: 'Nilai Ideal Kepentingan',
                dataIndex: 'kepentingan_positif',
                align: 'center'
            },
            {
                title: 'Nilai Ideal Ketersediaan',
                dataIndex: 'ketersediaan_positif',
                align: 'center'
            }
        ]
        // Ideal Negatif
        this.columnsDataIdealNegatif = [
            {
                title: 'Solusi Ideal',
                render: () => (
                    <Space size="middle">
                        Ideal Negatif
                    </Space>
                )
            },
            {
                title: 'Nilai Ideal Harga',
                dataIndex: 'harga_negatif',
                align: 'center'
            },
            {
                title: 'Nilai Ideal Stok',
                dataIndex: 'stok_negatif',
                align: 'center'
            },
            {
                title: 'Nilai Ideal Kepentingan',
                dataIndex: 'kepentingan_negatif',
                align: 'center'
            },
            {
                title: 'Nilai Ideal Ketersediaan',
                dataIndex: 'ketersediaan_negatif',
                align: 'center'
            }
        ]
        // Jarak
        this.columnsDataJarak = [
            {
                title: 'Nama Bahan Baku',
                dataIndex: 'nama_bahan_baku'
            },
            {
                title: 'Jarak Positif',
                dataIndex: 'jarak_positif',
                align: 'center'
            },
            {
                title: 'Jarak Negatif',
                dataIndex: 'jarak_negatif',
                align: 'center'
            },
            {
                title: 'Nilai Prefensi',
                dataIndex: 'nilai_prefensi',
                align: 'center'
            }
        ]
        // Hasil Prefensi
        this.columnsHasilPrefensi = [
            {
                title: 'Nama Bahan Baku',
                dataIndex: 'nama_bahan_baku'
            },
            {
                title: 'Nilai Prefensi',
                dataIndex: 'prefensi',
                align: 'center'
            },
            {
                title: 'Aksi',
                align: 'center',
                render: (record) => (
                    <Space size="middle">
                        <a onClick={() => this.props.history.push(`/saw/${record.id_bahan_baku}`)}>Lanjut Pemilihan Supplier</a>
                    </Space>
                ),
            },
        ]
    }

    static getDerivedStateFromProps (nextProps, prevState) {
        let updated = {}
        if (!_.isEqual(nextProps.topsisState, prevState.topsisState) && !_.isEmpty(nextProps.topsisState)) {
            updated = { ...updated, topsisState: nextProps.topsisState}
        }
        if (!_.isEqual(nextProps.topsisAddState, prevState.topsisAddState) && !_.isEmpty(nextProps.topsisAddState)) {
            updated = { ...updated, topsisAddState: nextProps.topsisAddState}
        }
        if (!_.isEmpty(updated)) {
          return updated
        }
        return null
    }

    modalAdd = () => {
        this.setState(prevState => ({
          modalAdd: !prevState.modalAdd
        }))
    }

    addData = values => {
        this.setState({
            addingData: true
        })
        this.props.topsisAddRequest()
        this.modalAdd()
    }

    modalAfterAdd = () => {
        this.setState(prevState => ({
          modalAfterAdd: !prevState.modalAfterAdd
        }))
    }

    modalCloseAfterAdd = () => {
        this.setState(prevState => ({
          modalAfterAdd: !prevState.modalAfterAdd
        }))
        this.props.topsisRequest(1)
    }

    changePage = (e) => {
        this.props.topsisRequest(e)
        this.setState({
            page: e
        })
    }

    componentDidMount () {
      const { page } = this.state
      this.props.topsisRequest(page)
    }

    componentDidUpdate () {
        const { topsisAddState, addingData } = this.state
        if (!topsisAddState.fetching && topsisAddState.success && !_.isEmpty(topsisAddState.data) && addingData) {
            this.setState({
                modalAfterAdd: true,
                addingData: false
            })
        }
    }

    render () {
        const { topsisState, topsisAddState, page, countData } = this.state
        console.log(topsisAddState)
        const data = !topsisState.fetching && topsisState.success && !_.isEmpty(topsisState.data) ? topsisState.data : {}
        if (!topsisState.fetching && topsisState.success) {
            let sizePage = data.data.page * 10
            return (
                <Layout  parent='menu-topsis' type='topsis'>
                    <div style={{textAlign: 'right'}}>
                        <Button type="secondary" onClick={this.modalAdd}>Tambah Data</Button>
                    </div>
                    <Table
                        style={{marginTop: 10}}
                        columns={this.columns}
                        dataSource={data.data.data}
                        pagination={{
                            defaultCurrent: page,
                            total: sizePage,
                            showSizeChanger: false,
                            onChange: this.changePage,
                        }}
                    />
                    {/* Tambah Data */}
                    <Modal
                        title='Memilih Bahan Baku'
                        centered
                        visible={this.state.modalAdd}
                        onCancel={this.modalAdd}
                        onOk={this.addData}
                    >
                        <div>Apakah anda ingin melakukan pemilihan bahan baku?</div>
                    </Modal>
                    {/* Tampil Data */}
                    {
                        !topsisAddState.fetching && topsisAddState.success && !_.isEmpty(topsisAddState.data) ? (
                            <Modal
                                title='Hasil Perhitungan'
                                centered
                                width={`auto`}
                                visible={this.state.modalAfterAdd}
                                onCancel={this.modalCloseAfterAdd}
                                onOk={this.modalCloseAfterAdd}
                                width={1280}
                            >
                                <div className='text-bolder'>Data Kriteria</div>
                                <Table
                                    style={{margin: '10px 0px'}}
                                    columns={this.columnsKriteria}
                                    dataSource={topsisAddState.data.data.dataTopsis.dataKriteria}
                                    pagination={{
                                        disabled: true,
                                        hideOnSinglePage: true
                                    }}
                                />
                                <div className='text-bolder'>Data Bahan Baku</div>
                                <Table
                                    style={{margin: '10px 0px'}}
                                    columns={this.columnsDataBarang}
                                    dataSource={topsisAddState.data.data.dataTopsis.dataBarang}
                                    pagination={{
                                        disabled: true,
                                        hideOnSinglePage: true
                                    }}
                                />
                                <div className='text-bolder'>Data Normalisasi</div>
                                <Table
                                    style={{margin: '10px 0px'}}
                                    columns={this.columnsDataNormalisasi}
                                    dataSource={topsisAddState.data.data.dataNormalisasi}
                                    pagination={{
                                        disabled: true,
                                        hideOnSinglePage: true
                                    }}
                                />
                                <div className='text-bolder'>Data Normalisasi Terbobot</div>
                                <Table
                                    style={{margin: '10px 0px'}}
                                    columns={this.columnsDataNormalisasiTerbobot}
                                    dataSource={topsisAddState.data.data.dataTerbobot}
                                    pagination={{
                                        disabled: true,
                                        hideOnSinglePage: true
                                    }}
                                />
                                <div className='text-bolder'>Data Ideal Positif</div>
                                <Table
                                    style={{margin: '10px 0px'}}
                                    columns={this.columnsDataIdealPositif}
                                    dataSource={topsisAddState.data.data.dataIdeal.idealPositif}
                                    pagination={{
                                        disabled: true,
                                        hideOnSinglePage: true
                                    }}
                                />
                                <div className='text-bolder'>Data Ideal Negatif</div>
                                <Table
                                    style={{margin: '10px 0px'}}
                                    columns={this.columnsDataIdealNegatif}
                                    dataSource={topsisAddState.data.data.dataIdeal.idealNegatif}
                                    pagination={{
                                        disabled: true,
                                        hideOnSinglePage: true
                                    }}
                                />
                                <div className='text-bolder'>Data Jarak</div>
                                <Table
                                    style={{margin: '10px 0px'}}
                                    columns={this.columnsDataJarak}
                                    dataSource={topsisAddState.data.data.jarak}
                                    pagination={{
                                        disabled: true,
                                        hideOnSinglePage: true
                                    }}
                                />
                                <div className='text-bolder'>Hasil Prefensi</div>
                                <Table
                                    style={{margin: '10px 0px'}}
                                    columns={this.columnsHasilPrefensi}
                                    dataSource={topsisAddState.data.data.alternatifPilihan}
                                    pagination={{
                                        disabled: true,
                                        hideOnSinglePage: true
                                    }}
                                />
                            </Modal>
                        ) : null
                    }
                </Layout>
            )
        } else {
            return <Loading />
        }
    }
}

const mapStateToProps = (state) => ({
    topsisState: state.topsisState,
    topsisAddState: state.topsisAddState
})

const mapDispatchToProps = (dispatch) => ({
    topsisRequest: (data) => dispatch(TopsisActions.topsisRequest(data)),
    topsisAddRequest: () => dispatch(TopsisAddActions.topsisAddRequest()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Topsis)