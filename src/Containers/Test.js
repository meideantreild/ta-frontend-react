import React, { Component } from 'react'
import { connect } from 'react-redux'

import Layout from '../Components/LayoutMenu'

class Test extends Component {
    render () {
        return (
            <Layout/>
        )
    }
}

const mapStateToProps = (state) => ({
})

const mapDispatchToProps = (dispatch) => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(Test)