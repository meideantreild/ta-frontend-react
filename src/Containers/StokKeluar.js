import React, { Component } from 'react'
import _, { concat } from 'lodash'
import { connect } from 'react-redux'
import moment from 'moment'
import { Table, Space, Button, Modal, Form, Row, Col, Input, Select, Cascader, InputNumber, DatePicker, message } from 'antd'

import Layout from '../Components/LayoutMenu'
import Loading from '../Components/Loading'
import { NumberWithCommas } from '../Utils'

// Redux
import StokKeluarActions from '../ReduxSaga/Redux/StokKeluarRedux'
import StokKeluarDetailActions from '../ReduxSaga/Redux/StokKeluarDetailRedux'
import BahanBakuDetailActions from '../ReduxSaga/Redux/BahanBakuDetailRedux'

const dateFormat = 'YYYY-MM-DD'
class StokKelaur extends Component {
    constructor (props) {
        super(props)
        this.state = {
            stokKeluarState: undefined,
            stokKeluarDetailState: undefined,
            bahanBakuDetailState: undefined,
            countData: null,
            page: 1,
            idDetail: null,
            modalAdd: false,
            modalDelete: false,
            isChoosingItem: false,
            isDisabledMinimum: false
        }
        this.columns = [
            {
                title: 'Bahan Baku',
                dataIndex: 'nama_bahan_baku',
            },
            {
                title: 'Jumlah',
                dataIndex: 'jumlah_stok_keluar',
            },
            {
                title: 'Stok',
                dataIndex: 'stok_bahan_baku',
                align: 'center',
            },
            {
                title: 'Tanggal',
                dataIndex: 'tanggal_keluar',
            },
            {
                title: 'Aksi',
                align: 'center',
                render: (record) => (
                    <Space size="middle">
                        <a onClick={() => this.getDeleteDetail(record.id_stok_keluar)}> Hapus Data</a>
                    </Space>
                ),
            },
        ]
    }

    static getDerivedStateFromProps (nextProps, prevState) {
        let updated = {}
        if (!_.isEqual(nextProps.stokKeluarState, prevState.stokKeluarState) && !_.isEmpty(nextProps.stokKeluarState)) {
            updated = { ...updated, stokKeluarState: nextProps.stokKeluarState}
        }
        if (!_.isEqual(nextProps.stokKeluarDetailState, prevState.stokKeluarDetailState) && !_.isEmpty(nextProps.stokKeluarDetailState)) {
            updated = { ...updated, stokKeluarDetailState: nextProps.stokKeluarDetailState }
        }
        if (!_.isEqual(nextProps.bahanBakuDetailState, prevState.bahanBakuDetailState) && !_.isEmpty(nextProps.bahanBakuDetailState)) {
            updated = { ...updated, bahanBakuDetailState: nextProps.bahanBakuDetailState }
        }
        if (!_.isEmpty(updated)) {
          return updated
        }
        return null
    }
    
    modalAdd = () => {
        this.setState(prevState => ({
          modalAdd: !prevState.modalAdd
        }))
    }

    modalDelete = () => {
        this.setState(prevState => ({
          modalDelete: !prevState.modalDelete
        }))
    }

    getDeleteDetail = (e) => {
        this.setState({
            idDetail: e
        })
        this.props.getStokKeluarDetailRequest(e)
        this.modalDelete()
    }

    handleMinimum = (e) => {
        this.props.getBahanBakuDetailRequest(e)
        this.setState({
            isChoosingItem: true
        })
    }

    addData = values => {
        this.setState({
            page: 1
        })
        let TanggalKeluar = moment(values.tanggal_keluar).format('YYYY-MM-DD')
        const payload = {
            id_bahan_baku: values.nama_bahan_baku[0],
            jumlah_stok_keluar: values.jumlah_stok_keluar,
            tanggal_keluar: TanggalKeluar
        }
        this.props.addStokKeluarRequest(payload)
        this.modalAdd()
    }

    deleteData = () => {
        this.setState({
            page: 1,
            countData: this.state.stokKeluarState.data.data.data.length
        })
        this.props.deleteStokKeluarRequest(this.state.idDetail)
        this.modalDelete()
    }  

    changePage = (e) => {
        this.props.getStokKeluarRequest(e)
        this.setState({
            page: e
        })
    }

    componentDidMount () {
      const { page } = this.state
      this.props.getStokKeluarRequest(page)
    }

    componentDidUpdate () {
        const { bahanBakuDetailState, isChoosingItem } = this.state
        if (!bahanBakuDetailState.fetchin && bahanBakuDetailState.success && isChoosingItem) {
            this.setState({
                isDisabledMinimum: true,
                isChoosingItem: false
            })
        }
    }

    render () {
        const { stokKeluarState, stokKeluarDetailState, bahanBakuDetailState, page, countData, isDisabledMinimum } = this.state
        let dataBahan = []
        const data = !stokKeluarState.fetching && stokKeluarState.success && !_.isEmpty(stokKeluarState.data) ? stokKeluarState.data : {}
        const dataDetail = !stokKeluarDetailState.fetching && stokKeluarDetailState.success && !_.isEmpty(stokKeluarDetailState.data) ? stokKeluarDetailState.data : {}
        if (!_.isEmpty(data)) {
            data.data.bahan.map((arrayData) =>
                dataBahan.push({
                    value: arrayData.id_bahan_baku,
                    label: arrayData.nama_bahan_baku
                })
            )
        }
        if (!stokKeluarState.fetching && stokKeluarState.success) {
            if (countData !== null) {
                if (countData === stokKeluarState.data.data.data.length) {
                    message.info('Data gagal dihapus!')
                } else {
                    message.info('Data berhasil dihapus')
                }
                this.setState({
                    countData: null
                })
            }
            let sizePage = data.data.page * 10
            return (
                <Layout  parent='menu-bahanbaku' type='stokkeluar'>
                    <div style={{textAlign: 'right'}}>
                        <Button type="secondary" onClick={this.modalAdd}>Tambah Data</Button>
                    </div>
                    <Table
                        style={{marginTop: 10}}
                        columns={this.columns}
                        dataSource={data.data.data}
                        pagination={{
                            defaultCurrent: page,
                            total: sizePage,
                            showSizeChanger: false,
                            onChange: this.changePage
                        }}
                    />
                    {/* Tambah Data */}
                    <Modal
                        title='Tambah Data Stok Keluar'
                        centered
                        visible={this.state.modalAdd}
                        onCancel={this.modalAdd}
                        footer={null}
                        width={520}
                    >
                        <Form
                            layout='vertical'
                            labelCol={{ span: 24 }}
                            onFinish={this.addData}
                        >
                            <Col className='gutter-row' span={24}>
                                <Form.Item
                                    label='Nama Bahan Baku'
                                    name='nama_bahan_baku'
                                    rules={[{
                                        required: true,
                                        message: 'Nama bahan baku harus dipilih!'
                                    }]}
                                >
                                    <Cascader options={dataBahan} onChange={this.handleMinimum}/>
                                </Form.Item>
                            </Col>
                            <Col className='gutter-row' span={24}>
                                <Form.Item
                                    label='Jumlah Barang Keluar'
                                    name='jumlah_stok_keluar'
                                    rules={
                                        [{
                                            required: true,
                                            message: 'Jumlah harus diisi!'
                                        },{
                                            type: 'number',
                                            max: isDisabledMinimum ? !_.isEmpty(bahanBakuDetailState.data) ? bahanBakuDetailState.data.stok_bahan_baku : 0 : 0,
                                            message: `Stok barang di gudang ${isDisabledMinimum ? !_.isEmpty(bahanBakuDetailState.data) ? bahanBakuDetailState.data.stok_bahan_baku : 0 : 0}`
                                        }]
                                    }
                                >
                                    <InputNumber disabled={!isDisabledMinimum}/>
                                </Form.Item>
                            </Col>
                            <Col className='gutter-row' span={24}>
                                <Form.Item
                                    label='Tanggal Keluar'
                                    name='tanggal_keluar'
                                    rules={
                                        [{
                                            required: true,
                                            message: 'Tanggal keluar harus dipilih!'
                                        }]
                                    }
                                >
                                    <DatePicker style={{width: '100%'}} />
                                </Form.Item>
                            </Col>
                            <Form.Item
                                style={{marginBottom: 0, textAlign: 'right'}}
                            >
                                <Button type="primary" htmlType="submit">
                                    Simpan Data
                                </Button>
                            </Form.Item>
                        </Form>
                    </Modal>
                    {/* Delete Data */}
                    <Modal
                        title='Hapus Data Stok Keluar'
                        centered
                        visible={this.state.modalDelete}
                        onCancel={this.modalDelete}
                        onOk={this.deleteData}
                    >
                    {
                        !_.isEmpty(dataDetail) ? (
                            <div>Apakah anda yakin ingin menghapus stok keluar pada tanggal {dataDetail.tanggal_keluar}?</div>
                        ) : <div>Mengambil data...</div>
                    }   
                    </Modal>
                </Layout>
            )
        } else {
            return <Loading />    
        }
    }
}

const mapStateToProps = (state) => ({
    stokKeluarState: state.stokKeluarState,
    stokKeluarDetailState: state.stokKeluarDetailState,
    bahanBakuDetailState: state.bahanBakuDetailState
})

const mapDispatchToProps = (dispatch) => ({
    getStokKeluarRequest: (data) => dispatch(StokKeluarActions.getStokKeluarRequest(data)),
    getStokKeluarDetailRequest: (data) => dispatch(StokKeluarDetailActions.getStokKeluarDetailRequest(data)),
    addStokKeluarRequest: (data) => dispatch(StokKeluarActions.addStokKeluarRequest(data)),
    deleteStokKeluarRequest: (data) => dispatch(StokKeluarActions.deleteStokKeluarRequest(data)),
    getBahanBakuDetailRequest: (data) => dispatch(BahanBakuDetailActions.getBahanBakuDetailRequest(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(StokKelaur)