import React, { Component } from 'react'
import _ from 'lodash'
import { connect } from 'react-redux'
import moment from 'moment'
import { Table, Space, Button, Modal, Form, Row, Col, Input, Select, Cascader, InputNumber, DatePicker, message } from 'antd'

import Layout from '../Components/LayoutMenu'
import Loading from '../Components/Loading'
import { NumberWithCommas } from '../Utils'

// Redux
import StokMasukActions from '../ReduxSaga/Redux/StokMasukRedux'
import StokMasukDetailActions from '../ReduxSaga/Redux/StokMasukDetailRedux'
import BahanBakuDetailActions from '../ReduxSaga/Redux/BahanBakuDetailRedux'

const dateFormat = 'YYYY-MM-DD'
class StokMasuk extends Component {
    constructor (props) {
        super(props)
        this.state = {
            stokMasukState: undefined,
            stokMasukDetailState: undefined,
            bahanBakuDetailState: undefined,
            countData: null,
            page: 1,
            idDetail: null,
            modalAdd: false,
            modalDelete: false,
            isChoosingItem: false,
            isDisabledMinimum: false
        }
        this.columns = [
            {
                title: 'Supplier',
                dataIndex: 'nama_supplier',
            },
            {
                title: 'Bahan Baku',
                dataIndex: 'nama_bahan_baku',
            },
            {
                title: 'Harga Beli',
                dataIndex: 'harga_beli',
                render: (value) => (
                  <Space size="middle">
                      Rp {NumberWithCommas(value)}
                  </Space>
                ),
            },
            {
                title: 'Jumlah',
                dataIndex: 'jumlah_stok_masuk',
            },
            {
                title: 'Stok',
                dataIndex: 'stok_bahan_baku',
                align: 'center',
            },
            {
                title: 'Tanggal',
                dataIndex: 'tanggal_masuk',
            },
            {
                title: 'Aksi',
                align: 'center',
                render: (record) => (
                    <Space size="middle">
                        <a onClick={() => this.getDeleteDetail(record.id_stok_masuk)}> Hapus Data</a>
                    </Space>
                ),
            },
        ]
    }

    static getDerivedStateFromProps (nextProps, prevState) {
        let updated = {}
        if (!_.isEqual(nextProps.stokMasukState, prevState.stokMasukState) && !_.isEmpty(nextProps.stokMasukState)) {
            updated = { ...updated, stokMasukState: nextProps.stokMasukState}
        }
        if (!_.isEqual(nextProps.stokMasukDetailState, prevState.stokMasukDetailState) && !_.isEmpty(nextProps.stokMasukDetailState)) {
            updated = { ...updated, stokMasukDetailState: nextProps.stokMasukDetailState }
        }
        if (!_.isEqual(nextProps.bahanBakuDetailState, prevState.bahanBakuDetailState) && !_.isEmpty(nextProps.bahanBakuDetailState)) {
            updated = { ...updated, bahanBakuDetailState: nextProps.bahanBakuDetailState }
        }
        if (!_.isEmpty(updated)) {
          return updated
        }
        return null
    }

    modalAdd = () => {
        this.setState(prevState => ({
          modalAdd: !prevState.modalAdd
        }))
    }

    modalDelete = () => {
        this.setState(prevState => ({
          modalDelete: !prevState.modalDelete
        }))
    }

    getDetail = (e) => {
        this.setState({
            idDetail: e
        })
        this.props.getStokMasukDetailRequest(e)
        this.modalEdit()
    }

    getDeleteDetail = (e) => {
        this.setState({
            idDetail: e
        })
        this.props.getStokMasukDetailRequest(e)
        this.modalDelete()
    }

    handleMinimum = (e) => {
        this.props.getBahanBakuDetailRequest(e)
        this.setState({
            isChoosingItem: true
        })
    }

    addData = values => {
        this.setState({
            page: 1
        })
        let tanggalMasuk = moment(values.tanggal_masuk).format('YYYY-MM-DD')
        const payload = {
            id_bahan_baku: values.nama_bahan_baku[0],
            id_supplier: values.nama_supplier[0],
            harga_beli: values.harga_beli,
            jumlah_stok_masuk: values.jumlah_stok_masuk,
            tanggal_masuk: tanggalMasuk
        }
        this.props.addStokMasukRequest(payload)
        this.modalAdd()
    }

    deleteData = () => {
        this.setState({
            page: 1,
            countData: this.state.stokMasukState.data.data.data.length
        })
        this.props.deleteStokMasukRequest(this.state.idDetail)
        this.modalDelete()
    }  

    changePage = (e) => {
        this.props.getStokMasukRequest(e)
        this.setState({
            page: e
        })
    }

    componentDidMount () {
      const { page } = this.state
      this.props.getStokMasukRequest(page)
    }

    componentDidUpdate () {
        const { bahanBakuDetailState, isChoosingItem } = this.state
        if (!bahanBakuDetailState.fetchin && bahanBakuDetailState.success && isChoosingItem) {
            this.setState({
                isDisabledMinimum: true,
                isChoosingItem: false
            })
        }
    }

    render () {
        const { stokMasukState, stokMasukDetailState, bahanBakuDetailState, page, countData, isDisabledMinimum } = this.state
        let dataBahan = []
        let dataSupplier = []
        const data = !stokMasukState.fetching && stokMasukState.success && !_.isEmpty(stokMasukState.data) ? stokMasukState.data : {}
        const dataDetail = !stokMasukDetailState.fetching && stokMasukDetailState.success && !_.isEmpty(stokMasukDetailState.data) ? stokMasukDetailState.data : {}
        if (!_.isEmpty(data)) {
            data.data.bahan.map((arrayData) =>
                dataBahan.push({
                    value: arrayData.id_bahan_baku,
                    label: arrayData.nama_bahan_baku
                })
            )
            data.data.supplier.map((arrayData) =>
                dataSupplier.push({
                    value: arrayData.id_supplier,
                    label: arrayData.nama_supplier
                })
            )
        }
        if (!stokMasukState.fetching && stokMasukState.success) {
            if (countData !== null) {
                if (countData === stokMasukState.data.data.data.length) {
                    message.info('Data gagal dihapus!')
                } else {
                    message.info('Data berhasil dihapus')
                }
                this.setState({
                    countData: null
                })
            }
            let sizePage = data.data.page * 10
            return (
                <Layout  parent='menu-bahanbaku' type='stokmasuk'>
                    <div style={{textAlign: 'right'}}>
                        <Button type="secondary" onClick={this.modalAdd}>Tambah Data</Button>
                    </div>
                    <Table
                        style={{marginTop: 10}}
                        columns={this.columns}
                        dataSource={data.data.data}
                        pagination={{
                            defaultCurrent: page,
                            total: sizePage,
                            showSizeChanger: false,
                            onChange: this.changePage
                        }}
                    />
                    {/* Tambah Data */}
                    <Modal
                        title='Tambah Data Stok Masuk'
                        centered
                        visible={this.state.modalAdd}
                        onCancel={this.modalAdd}
                        footer={null}
                        width={720}
                    >
                        <Form
                            layout='vertical'
                            labelCol={{ span: 24 }}
                            onFinish={this.addData}
                        >
                        <Row gutter={12}>
                            <Col className='gutter-row' span={12}>
                                <Form.Item
                                    label='Nama Bahan Baku'
                                    name='nama_bahan_baku'
                                    rules={[{
                                        required: true,
                                        message: 'Nama bahan baku harus dipilih!'
                                    }]}
                                >
                                    <Cascader options={dataBahan} onChange={this.handleMinimum}/>
                                </Form.Item>
                            </Col>
                            <Col className='gutter-row' span={12}>
                                <Form.Item
                                    label='Supplier'
                                    name='nama_supplier'
                                    rules={[{
                                        required: true,
                                        message: 'Supplier harus dipilih!'
                                    }]}
                                >
                                    <Cascader options={dataSupplier}/>
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={12}>
                            <Col className='gutter-row' span={12}>
                                <Form.Item
                                    label='Harga Beli'
                                    name='harga_beli'
                                    rules={
                                        [{
                                            required: true,
                                            message: 'Harga beli harus diisi!'
                                        },{
                                            type: 'number',
                                            min: 0,
                                            message: 'Format harga beli adalah angka positif!'
                                        }]
                                    }
                                >
                                    <InputNumber/>
                                </Form.Item>
                            </Col>
                            <Col className='gutter-row' span={12}>
                                <Form.Item
                                    label='Jumlah Barang Masuk'
                                    name='jumlah_stok_masuk'
                                    rules={
                                        [{
                                            required: true,
                                            message: 'Jumlah harus diisi!'
                                        },{
                                            type: 'number',
                                            min: isDisabledMinimum ? !_.isEmpty(bahanBakuDetailState.data) ? bahanBakuDetailState.data.minimal_pembelian : 0 : 0,
                                            message: `Minimal jumlah pembelian adalah ${isDisabledMinimum ? !_.isEmpty(bahanBakuDetailState.data) ? bahanBakuDetailState.data.minimal_pembelian : 0 : 0}`
                                        }]
                                    }
                                >
                                    <InputNumber disabled={!isDisabledMinimum}/>
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={12}>
                            <Col className='gutter-row' span={12}>
                                <Form.Item
                                    label='Tanggal Masuk'
                                    name='tanggal_masuk'
                                    rules={
                                        [{
                                            required: true,
                                            message: 'Tanggal masuk harus dipilih!'
                                        }]
                                    }
                                >
                                    <DatePicker style={{width: '100%'}} />
                                </Form.Item>
                            </Col>
                        </Row>
                            <Form.Item
                                style={{marginBottom: 0, textAlign: 'right'}}
                            >
                                <Button type="primary" htmlType="submit">
                                    Simpan Data
                                </Button>
                            </Form.Item>
                        </Form>
                    </Modal>
                    {/* Delete Data */}
                    <Modal
                        title='Hapus Data Stok Masuk'
                        centered
                        visible={this.state.modalDelete}
                        onCancel={this.modalDelete}
                        onOk={this.deleteData}
                    >
                        {
                            !_.isEmpty(dataDetail) ? (
                                <div>Apakah anda yakin ingin menghapus stok masuk pada tanggal {dataDetail.tanggal_masuk}?</div>
                            ) : <div>Mengambil data...</div>
                        }   
                    </Modal>
                </Layout>
            )
        } else {
            return <Loading />
        }
    }
}

const mapStateToProps = (state) => ({
    stokMasukState: state.stokMasukState,
    stokMasukDetailState: state.stokMasukDetailState,
    bahanBakuDetailState: state.bahanBakuDetailState
})

const mapDispatchToProps = (dispatch) => ({
    getStokMasukRequest: (data) => dispatch(StokMasukActions.getStokMasukRequest(data)),
    getStokMasukDetailRequest: (data) => dispatch(StokMasukDetailActions.getStokMasukDetailRequest(data)),
    addStokMasukRequest: (data) => dispatch(StokMasukActions.addStokMasukRequest(data)),
    deleteStokMasukRequest: (data) => dispatch(StokMasukActions.deleteStokMasukRequest(data)),
    getBahanBakuDetailRequest: (data) => dispatch(BahanBakuDetailActions.getBahanBakuDetailRequest(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(StokMasuk)