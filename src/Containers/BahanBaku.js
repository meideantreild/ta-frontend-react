import React, { Component } from 'react'
import _ from 'lodash'
import { connect } from 'react-redux'
import { Table, Space, Button, Modal, Form, Row, Col, Input, InputNumber, message } from 'antd'

import Layout from '../Components/LayoutMenu'
import Loading from '../Components/Loading'
import { NumberWithCommas } from '../Utils'

// Redux
import BahanBakuActions from '../ReduxSaga/Redux/BahanBakuRedux'
import BahanBakuDetailActions from '../ReduxSaga/Redux/BahanBakuDetailRedux'

class BahanBaku extends Component {
    constructor (props) {
        super(props)
        this.state = {
            bahanBakuState: undefined,
            bahanBakuDetailState: undefined,
            countData: null,
            page: 1,
            idDetail: null,
            modalAdd: false,
            modalEdit: false,
            modalDelete: false,
        }
        this.columns = [
            {
                title: 'Nama Bahan',
                dataIndex: 'nama_bahan_baku',
            },
            {
                title: 'Harga Terakhir',
                dataIndex: 'harga_terakhir',
                render: (value) => (
                  <Space size="middle">
                      Rp {NumberWithCommas(value)}
                  </Space>
                ),
            },
            {
                title: 'Stok',
                dataIndex: 'stok_bahan_baku',
                align: 'center',
            },
            {
                title: 'Satuan',
                dataIndex: 'satuan_bahan_baku',
                align: 'center',
            },
            {
                title: 'Minimal',
                dataIndex: 'minimal_pembelian',
                align: 'center',
            },
            {
                title: 'Aksi',
                align: 'center',
                render: (record) => (
                    <Space size="middle">
                        <a onClick={() => this.getDetail(record.id_bahan_baku)}>Ubah Data</a>
                        <a onClick={() => this.getDeleteDetail(record.id_bahan_baku)}> Hapus Data</a>
                    </Space>
                ),
            },
        ]
    }

    static getDerivedStateFromProps (nextProps, prevState) {
        let updated = {}
        if (!_.isEqual(nextProps.bahanBakuState, prevState.bahanBakuState) && !_.isEmpty(nextProps.bahanBakuState)) {
            updated = { ...updated, bahanBakuState: nextProps.bahanBakuState }
        }
        if (!_.isEqual(nextProps.bahanBakuDetailState, prevState.bahanBakuDetailState) && !_.isEmpty(nextProps.bahanBakuDetailState)) {
            updated = { ...updated, bahanBakuDetailState: nextProps.bahanBakuDetailState }
        }
        if (!_.isEmpty(updated)) {
          return updated
        }
        return null
    }

    modalAdd = () => {
        this.setState(prevState => ({
          modalAdd: !prevState.modalAdd
        }))
    }

    modalEdit = () => {
        this.setState(prevState => ({
          modalEdit: !prevState.modalEdit
        }))
    }

    modalDelete = () => {
        this.setState(prevState => ({
          modalDelete: !prevState.modalDelete
        }))
    }

    getDetail = (e) => {
        this.setState({
            idDetail: e
        })
        this.props.getBahanBakuDetailRequest(e)
        this.modalEdit()
    }

    getDeleteDetail = (e) => {
        this.setState({
            idDetail: e
        })
        this.props.getBahanBakuDetailRequest(e)
        this.modalDelete()
    }

    addData = values => {
        this.setState({
            page: 1
        })
        this.props.addBahanBakuRequest(values)
        this.modalAdd()
    }

    editData = values => {
        this.setState({
            page: 1
        })
        message.info('Data berhasil diubah')
        this.props.updateBahanBakuRequest(values, this.state.idDetail)
        this.modalEdit()
    }

    deleteData = () => {
        this.setState({
            page: 1,
            countData: this.state.bahanBakuState.data.data.data.length
        })
        this.props.deleteBahanBakuRequest(this.state.idDetail)
        this.modalDelete()
    }  

    changePage = (e) => {
        this.props.getBahanBakuRequest(e)
        this.setState({
            page: e
        })
    }

    componentDidMount () {
      const { page } = this.state
      this.props.getBahanBakuRequest(page)
    }

    render () {
        const { bahanBakuState, bahanBakuDetailState, page, countData } = this.state
        const data = !bahanBakuState.fetching && bahanBakuState.success && !_.isEmpty(bahanBakuState.data) ? bahanBakuState.data : {}
        const dataDetail = !bahanBakuDetailState.fetching && bahanBakuDetailState.success && !_.isEmpty(bahanBakuDetailState.data) ? bahanBakuDetailState.data : {}
        if (!bahanBakuState.fetching && bahanBakuState.success) {
            if (countData !== null) {
                if (countData === bahanBakuState.data.data.data.length) {
                    message.info('Data gagal dihapus!')
                } else {
                    message.info('Data berhasil dihapus')
                }
                this.setState({
                    countData: null
                })
            }
            let sizePage = data.data.page * 10
            return (
                <Layout  parent='menu-bahanbaku' type='bahanbaku'>
                    <div style={{textAlign: 'right'}}>
                        <Button type="secondary" onClick={this.modalAdd}>Tambah Data</Button>
                    </div>
                    <Table
                        style={{marginTop: 10}}
                        columns={this.columns}
                        dataSource={data.data.data}
                        pagination={{
                            defaultCurrent: page,
                            total: sizePage,
                            showSizeChanger: false,
                            onChange: this.changePage
                        }}
                    />
                    {/* Tambah Data */}
                    <Modal
                        title='Tambah Data Bahan Baku'
                        centered
                        visible={this.state.modalAdd}
                        onCancel={this.modalAdd}
                        footer={null}
                        width={720}
                    >
                        <Form
                            layout='vertical'
                            labelCol={{ span: 24 }}
                            onFinish={this.addData}
                        >
                        <Row gutter={12}>
                            <Col className='gutter-row' span={12}>
                                <Form.Item
                                    label='Nama Bahan Baku'
                                    name='nama_bahan_baku'
                                    rules={[{
                                        required: true,
                                        message: 'Nama bahan baku harus diisi!'
                                    }]}
                                >
                                    <Input />
                                </Form.Item>
                            </Col>
                            <Col className='gutter-row' span={12}>
                                <Form.Item
                                    label='Harga Terakhir'
                                    name='harga_terakhir'
                                    rules={
                                        [{
                                            required: true,
                                            message: 'Harga terakhir harus diisi!'
                                        },{
                                            type: 'number',
                                            message: 'Format harga terakhir adalah angka!'
                                        }]
                                    }
                                >
                                    <InputNumber/>
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={12}>
                            <Col className='gutter-row' span={12}>
                                <Form.Item
                                    label='Stok Gudang'
                                    name='stok_bahan_baku'
                                    rules={
                                        [{
                                            required: true,
                                            message: 'Stok gudang harus diisi!'
                                        },{
                                            type: 'number',
                                            message: 'Format stok gudang adalah angka!'
                                        }]
                                    }
                                >
                                    <InputNumber/>
                                </Form.Item>
                            </Col>
                            <Col className='gutter-row' span={12}>
                                <Form.Item
                                    label='Satuan'
                                    name='satuan_bahan_baku'
                                    rules={[{
                                        required: true,
                                        message: 'Satuan bahan baku harus diisi!'
                                    }]}
                                >
                                    <Input/>
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={12}>
                            <Col className='gutter-row' span={12}>
                                <Form.Item
                                    label='Minimal Pembelian'
                                    name='minimal_pembelian'
                                    rules={
                                        [{
                                            required: true,
                                            message: 'Minimal pembelian harus diisi!'
                                        },{
                                            type: 'number',
                                            message: 'Format minimal pembelian adalah angka!'
                                        }]
                                    }
                                >
                                    <InputNumber/>
                                </Form.Item>
                            </Col>
                            <Col className='gutter-row' span={12}>
                                <Form.Item
                                    label='Ketersediaan Pasar'
                                    name='ketersediaan'
                                    rules={
                                        [{
                                            required: true,
                                            message: 'Ketersediaan pasar harus diisi!'
                                        },{
                                            type: 'number',
                                            min: 1,
                                            max: 5,
                                            message: 'Format ketersediaan pasar adalah angka (min:1, max:5)!',
                                        }]
                                    }
                                >
                                    <InputNumber/>
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={12}>
                            <Col className='gutter-row' span={12}>
                                <Form.Item
                                    label='Kepentingan Bahan Baku'
                                    name='kepentingan_bahan_baku'
                                    rules={
                                        [{
                                            required: true,
                                            message: 'Kepentingan bahan harus diisi!'
                                        },{
                                            type: 'number',
                                            min: 1,
                                            max: 5,
                                            message: 'Format kepentingan bahan adalah angka (min:1, max:5)!'
                                        }]
                                    }
                                >
                                    <InputNumber/>
                                </Form.Item>
                            </Col>
                            <Col className='gutter-row' span={12}>
                                <Form.Item
                                    label='Standar Produksi'
                                    name='standart_produksi'
                                    rules={
                                        [{
                                            required: true,
                                            message: 'Standar Produksi bahan harus diisi!'
                                        },{
                                            type: 'number',
                                            message: 'Format standar produksi adalah angka!'
                                        }]
                                    }
                                >
                                    <InputNumber/>
                                </Form.Item>
                            </Col>
                        </Row>
                            <Form.Item
                                style={{marginBottom: 0, textAlign: 'right'}}
                            >
                                <Button type="primary" htmlType="submit">
                                    Simpan Data
                                </Button>
                            </Form.Item>
                        </Form>
                    </Modal>
                    {/* Edit Data */}
                    <Modal
                        title='Ubah Data Bahan Baku'
                        centered
                        visible={this.state.modalEdit}
                        onCancel={this.modalEdit}
                        footer={null}
                        width={720}
                    >
                        {
                            !_.isEmpty(dataDetail) ? (
                                <Form
                                    layout='vertical'
                                    labelCol={{ span: 24 }}
                                    onFinish={this.editData}
                                >
                                    <Row gutter={12}>
                                        <Col className='gutter-row' span={12}>
                                            <Form.Item
                                                label='Nama Bahan Baku'
                                                name='nama_bahan_baku'
                                                rules={[{
                                                    required: true,
                                                    message: 'Nama bahan baku harus diisi!'
                                                }]}
                                                initialValue={dataDetail.nama_bahan_baku}
                                            >
                                                <Input/>
                                            </Form.Item>
                                        </Col>
                                        <Col className='gutter-row' span={12}>
                                            <Form.Item
                                                label='Harga Terakhir'
                                                name='harga_terakhir'
                                                rules={
                                                    [{
                                                        required: true,
                                                        message: 'Harga terakhir harus diisi!'
                                                    },{
                                                        type: 'number',
                                                        message: 'Format harga terakhir adalah angka!'
                                                    }]
                                                }
                                                initialValue={dataDetail.harga_terakhir}
                                            >
                                                <InputNumber/>
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                    <Row gutter={12}>
                                        <Col className='gutter-row' span={12}>
                                            <Form.Item
                                                label='Stok Gudang'
                                                name='stok_bahan_baku'
                                                rules={
                                                    [{
                                                        required: true,
                                                        message: 'Stok gudang harus diisi!'
                                                    },{
                                                        type: 'number',
                                                        message: 'Format stok gudang adalah angka!'
                                                    }]
                                                }
                                                initialValue={dataDetail.stok_bahan_baku}
                                            >
                                                <InputNumber/>
                                            </Form.Item>
                                        </Col>
                                        <Col className='gutter-row' span={12}>
                                            <Form.Item
                                                label='Satuan'
                                                name='satuan_bahan_baku'
                                                rules={[{
                                                    required: true,
                                                    message: 'Satuan bahan baku harus diisi!'
                                                }]}
                                                initialValue={dataDetail.satuan_bahan_baku}
                                            >
                                                <Input/>
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                    <Row gutter={12}>
                                        <Col className='gutter-row' span={12}>
                                            <Form.Item
                                                label='Minimal Pembelian'
                                                name='minimal_pembelian'
                                                rules={
                                                    [{
                                                        required: true,
                                                        message: 'Minimal pembelian harus diisi!'
                                                    },{
                                                        type: 'number',
                                                        message: 'Format minimal pembelian adalah angka!'
                                                    }]
                                                }
                                                initialValue={dataDetail.minimal_pembelian}
                                            >
                                                <InputNumber/>
                                            </Form.Item>
                                        </Col>
                                        <Col className='gutter-row' span={12}>
                                            <Form.Item
                                                label='Ketersediaan Pasar'
                                                name='ketersediaan'
                                                rules={
                                                    [{
                                                        required: true,
                                                        message: 'Ketersediaan pasar harus diisi!'
                                                    },{
                                                        type: 'number',
                                                        min: 1,
                                                        max: 5,
                                                        message: 'Format ketersediaan pasar adalah angka (min:1, max:5)!',
                                                    }]
                                                }
                                                initialValue={dataDetail.ketersediaan}
                                            >
                                                <InputNumber/>
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                    <Row gutter={12}>
                                        <Col className='gutter-row' span={12}>
                                            <Form.Item
                                                label='Kepentingan Bahan Baku'
                                                name='kepentingan_bahan_baku'
                                                rules={
                                                    [{
                                                        required: true,
                                                        message: 'Kepentingan bahan harus diisi!'
                                                    },{
                                                        type: 'number',
                                                        min: 1,
                                                        max: 5,
                                                        message: 'Format kepentingan bahan adalah angka (min:1, max:5)!'
                                                    }]
                                                }
                                                initialValue={dataDetail.kepentingan_bahan_baku}
                                            >
                                                <InputNumber/>
                                            </Form.Item>
                                        </Col>
                                        <Col className='gutter-row' span={12}>
                                            <Form.Item
                                                label='Standar Produksi'
                                                name='standart_produksi'
                                                rules={
                                                    [{
                                                        required: true,
                                                        message: 'Standar Produksi bahan harus diisi!'
                                                    },{
                                                        type: 'number',
                                                        message: 'Format standar produksi adalah angka!'
                                                    }]
                                                }
                                                initialValue={dataDetail.standart_produksi}
                                            >
                                                <InputNumber/>
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                    <Form.Item
                                        style={{marginBottom: 0, textAlign: 'right'}}
                                    >
                                        <Button type="primary" htmlType="submit">
                                            Ubah Data
                                        </Button>
                                    </Form.Item>
                                </Form>
                            ) : <div>Mengambil data...</div>
                        }
                    </Modal>
                    {/* Delete Data */}
                    <Modal
                        title='Ubah Data Bahan Baku'
                        centered
                        visible={this.state.modalDelete}
                        onCancel={this.modalDelete}
                        onOk={this.deleteData}
                    >
                        {
                            !_.isEmpty(dataDetail) ? (
                                <div>Apakah anda yakin ingin menghapus bahan baku {dataDetail.nama_bahan_baku}?</div>
                            ) : <div>Mengambil data...</div>
                        }   
                    </Modal>
                </Layout>
            )
        } else {
            return <Loading />    
        }
    }
}

const mapStateToProps = (state) => ({
    bahanBakuState: state.bahanBakuState,
    bahanBakuDetailState: state.bahanBakuDetailState
})

const mapDispatchToProps = (dispatch) => ({
    getBahanBakuRequest: (data) => dispatch(BahanBakuActions.getBahanBakuRequest(data)),
    getBahanBakuDetailRequest: (data) => dispatch(BahanBakuDetailActions.getBahanBakuDetailRequest(data)),
    addBahanBakuRequest: (data) => dispatch(BahanBakuActions.addBahanBakuRequest(data)),
    updateBahanBakuRequest: (data, id) => dispatch(BahanBakuActions.updateBahanBakuRequest(data, id)),
    deleteBahanBakuRequest: (data) => dispatch(BahanBakuActions.deleteBahanBakuRequest(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(BahanBaku)