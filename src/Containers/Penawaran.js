import React, { Component } from 'react'
import _ from 'lodash'
import { connect } from 'react-redux'
import moment from 'moment'
import { Table, Space, Button, Modal, Form, Row, Col, Input, Cascader, InputNumber, DatePicker, message } from 'antd'

import Layout from '../Components/LayoutMenu'
import Loading from '../Components/Loading'
import { NumberWithCommas } from '../Utils'

// Redux
import PenawaranActions from '../ReduxSaga/Redux/PenawaranRedux'
import PenawaranDetailActions from '../ReduxSaga/Redux/PenawaranDetailRedux'

const dateFormat = 'YYYY-MM-DD'
class Penawaran extends Component {
    constructor (props) {
        super(props)
        this.state = {
            penawaranState: undefined,
            penawaranDetailState: undefined,
            countData: null,
            page: 1,
            idDetail: null,
            modalAdd: false,
            modalEdit: false,
            modalDelete: false,
        }
        this.columns = [
            {
                title: 'Nama Supplier',
                dataIndex: 'nama_supplier',
            },
            {
                title: 'Nama Bahan Baku',
                dataIndex: 'nama_bahan_baku',
            },
            {
                title: 'Harga Penawaran',
                dataIndex: 'harga_penawaran',
                render: (value) => (
                  <Space size="middle">
                      Rp {NumberWithCommas(value)}
                  </Space>
                ),
                align: 'center'
            },
            {
                title: 'Ongkos Penawaran',
                dataIndex: 'ongkos_penawaran',
                render: (value) => (
                  <Space size="middle">
                      Rp {NumberWithCommas(value)}
                  </Space>
                ),
                align: 'center'
            },
            {
                title: 'Kualitas Penawaran',
                dataIndex: 'kualitas_penawaran',
                align: 'center'
            },
            {
                title: 'Stok Penawaran',
                dataIndex: 'stok_penawaran',
                align: 'center'
            },
            {
                title: 'Tanggal Penawaran',
                dataIndex: 'tanggal_penawaran',
                align: 'center'
            },
            {
                title: 'Aksi',
                align: 'center',
                render: (record) => (
                    <Space size="middle">
                        <a onClick={() => this.getDetail(record.id_penawaran)}>Ubah Data</a>
                        <a onClick={() => this.getDeleteDetail(record.id_penawaran)}> Hapus Data</a>
                    </Space>
                )
            },
        ]
    }

    static getDerivedStateFromProps (nextProps, prevState) {
        let updated = {}
        if (!_.isEqual(nextProps.penawaranState, prevState.penawaranState) && !_.isEmpty(nextProps.penawaranState)) {
            updated = { ...updated, penawaranState: nextProps.penawaranState }
        }
        if (!_.isEqual(nextProps.penawaranDetailState, prevState.penawaranDetailState) && !_.isEmpty(nextProps.penawaranDetailState)) {
            updated = { ...updated, penawaranDetailState: nextProps.penawaranDetailState }
        }
        if (!_.isEmpty(updated)) {
          return updated
        }
        return null
    }

    modalAdd = () => {
        this.setState(prevState => ({
          modalAdd: !prevState.modalAdd
        }))
    }

    modalEdit = () => {
        this.setState(prevState => ({
          modalEdit: !prevState.modalEdit
        }))
    }

    modalDelete = () => {
        this.setState(prevState => ({
          modalDelete: !prevState.modalDelete
        }))
    }

    getDetail = (e) => {
        this.setState({
            idDetail: e
        })
        this.props.getPenawaranDetailRequest(e)
        this.modalEdit()
    }

    getDeleteDetail = (e) => {
        this.setState({
            idDetail: e
        })
        this.props.getPenawaranDetailRequest(e)
        this.modalDelete()
    }

    addData = values => {
        this.setState({
            page: 1
        })
        let tanggalPenawaran = moment(values.tanggal_penawaran).format('YYYY-MM-DD')
        const payload = {
            id_bahan_baku: values.nama_bahan_baku[0],
            id_supplier: values.nama_supplier[0],
            harga_penawaran: values.harga_penawaran,
            ongkos_penawaran: values.ongkos_penawaran,
            kualitas_penawaran: values.kualitas_penawaran[0],
            stok_penawaran: values.stok_penawaran,
            tanggal_penawaran: tanggalPenawaran
        }
        message.info('Penawaran berhasil ditambahkan')
        this.props.addPenawaranRequest(payload)
        this.modalAdd()
    }

    editData = values => {
        this.setState({
            page: 1
        })
        let tanggalPenawaran = moment(values.tanggal_penawaran).format('YYYY-MM-DD')
        const payload = {
            id_bahan_baku: values.nama_bahan_baku[0],
            id_supplier: values.nama_supplier[0],
            harga_penawaran: values.harga_penawaran,
            ongkos_penawaran: values.ongkos_penawaran,
            kualitas_penawaran: values.kualitas_penawaran[0],
            stok_penawaran: values.stok_penawaran,
            tanggal_penawaran: tanggalPenawaran
        }
        message.info('Penawaran berhasil diubah')
        this.props.updatePenawaranRequest(payload, this.state.idDetail)
        this.modalEdit()
    }

    deleteData = () => {
        this.setState({
            page: 1,
            countData: this.state.penawaranState.data.data.data.length
        })
        this.props.deletePenawaranRequest(this.state.idDetail)
        this.modalDelete()
    }  

    changePage = (e) => {
        this.props.getPenawaranRequest(e)
        this.setState({
            page: e
        })
    }

    componentDidMount () {
      const { page } = this.state
      this.props.getPenawaranRequest(page)
    }

    render () {
        const { penawaranState, penawaranDetailState, page, countData } = this.state
        let dataBahan = []
        let dataSupplier = []
        let dataKualitas = [
            {
                value: 1,
                label: 1
            },
            {
                value: 2,
                label: 2
            },
            {
                value: 3,
                label: 3
            },
            {
                value: 4,
                label: 4
            },
            {
                value: 5,
                label: 5
            },
        ]
        const data = !penawaranState.fetching && penawaranState.success && !_.isEmpty(penawaranState.data) ? penawaranState.data : {}
        const dataDetail = !penawaranDetailState.fetching && penawaranDetailState.success && !_.isEmpty(penawaranDetailState.data) ? penawaranDetailState.data : {}
        if (!_.isEmpty(data)) {
            data.data.bahan.map((arrayData) =>
                dataBahan.push({
                    value: arrayData.id_bahan_baku,
                    label: arrayData.nama_bahan_baku
                })
            )
            data.data.supplier.map((arrayData) =>
                dataSupplier.push({
                    value: arrayData.id_supplier,
                    label: arrayData.nama_supplier
                })
            )
        }
        if (!penawaranState.fetching && penawaranState.success) {
            if (countData !== null) {
                if (countData === penawaranState.data.data.data.length) {
                    message.info('Data gagal dihapus!')
                } else {
                    message.info('Data berhasil dihapus')
                }
                this.setState({
                    countData: null
                })
            }
            let sizePage = data.data.page * 10
            return (
                <Layout  parent='menu-bahanbaku' type='penawaran'>
                    <div style={{textAlign: 'right'}}>
                        <Button type="secondary" onClick={this.modalAdd}>Tambah Data</Button>
                    </div>
                    <Table
                        style={{marginTop: 10}}
                        columns={this.columns}
                        dataSource={data.data.data}
                        pagination={{
                            defaultCurrent: page,
                            total: sizePage,
                            showSizeChanger: false,
                            onChange: this.changePage
                        }}
                    />
                    {/* Tambah Data */}
                    <Modal
                        title='Tambah Data Penawaran'
                        centered
                        visible={this.state.modalAdd}
                        onCancel={this.modalAdd}
                        footer={null}
                        width={720}
                    >
                        <Form
                            layout='vertical'
                            labelCol={{ span: 24 }}
                            onFinish={this.addData}
                        >
                            <Row gutter={12}>
                                <Col className='gutter-row' span={12}>
                                    <Form.Item
                                        label='Nama Supplier'
                                        name='nama_supplier'
                                        rules={[{
                                            required: true,
                                            message: 'Nama Supplier harus dipilih!'
                                        }]}
                                    >
                                        <Cascader options={dataSupplier}/>
                                    </Form.Item>
                                </Col>
                                <Col className='gutter-row' span={12}>
                                    <Form.Item
                                        label='Bahan Baku'
                                        name='nama_bahan_baku'
                                        rules={[{
                                            required: true,
                                            message: 'Bahan Baku harus dipilih!'
                                        }]}
                                    >
                                        <Cascader options={dataBahan}/>
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={12}>
                                <Col className='gutter-row' span={12}>
                                    <Form.Item
                                        label='Harga Penawaran'
                                        name='harga_penawaran'
                                        rules={
                                            [{
                                                required: true,
                                                message: 'Harga penawaran harus diisi!'
                                            },{
                                                type: 'number',
                                                min: 0,
                                                message: 'Format harga penawaran adalah angka positif!'
                                            }]
                                        }
                                    >
                                        <InputNumber />
                                    </Form.Item>
                                </Col>
                                <Col className='gutter-row' span={12}>
                                    <Form.Item
                                        label='Ongkos Penawaran'
                                        name='ongkos_penawaran'
                                        rules={
                                            [{
                                                required: true,
                                                message: 'Ongkos penawaran harus diisi!'
                                            },{
                                                type: 'number',
                                                min: 0,
                                                message: `Minimal Ongkos penawaran pembelian adalah 0`
                                            }]
                                        }
                                    >
                                        <InputNumber/>
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={12}>
                                <Col className='gutter-row' span={12}>
                                    <Form.Item
                                        label='Kualitas Penawaran'
                                        name='kualitas_penawaran'
                                        rules={
                                            [{
                                                required: true,
                                                message: 'Kualitas penawaran harus dipilih!'
                                            }]
                                        }
                                    >
                                        <Cascader options={dataKualitas}/>
                                    </Form.Item>
                                </Col>
                                <Col className='gutter-row' span={12}>
                                    <Form.Item
                                        label='Stok Penawaran'
                                        name='stok_penawaran'
                                        rules={
                                            [{
                                                required: true,
                                                message: 'Stok penawaran harus diisi!'
                                            },{
                                                type: 'number',
                                                min: 0,
                                                message: `Minimal stok penawaran pembelian adalah 0`
                                            }]
                                        }
                                    >
                                        <InputNumber/>
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Row gutter={12}>
                                <Col className='gutter-row' span={12}>
                                    <Form.Item
                                        label='Tanggal Penawaran'
                                        name='tanggal_penawaran'
                                        rules={
                                            [{
                                                required: true,
                                                message: 'Tanggal penawaran harus dipilih!'
                                            }]
                                        }
                                    >
                                        <DatePicker style={{width: '100%'}} />
                                    </Form.Item>
                                </Col>
                            </Row>
                            <Form.Item
                                style={{marginBottom: 0, textAlign: 'right'}}
                            >
                                <Button type="primary" htmlType="submit">
                                    Simpan Data
                                </Button>
                            </Form.Item>
                        </Form>
                    </Modal>
                    {/* Edit Data */}
                    <Modal
                        title='Ubah Data Bahan Baku'
                        centered
                        visible={this.state.modalEdit}
                        onCancel={this.modalEdit}
                        footer={null}
                        width={720}
                    >
                        {
                            !_.isEmpty(dataDetail) ? (
                                <Form
                                    layout='vertical'
                                    labelCol={{ span: 24 }}
                                    onFinish={this.editData}
                                >
                                    <Row gutter={12}>
                                        <Col className='gutter-row' span={12}>
                                            <Form.Item
                                                label='Nama Supplier'
                                                name='nama_supplier'
                                                rules={[{
                                                    required: true,
                                                    message: 'Nama Supplier harus dipilih!'
                                                }]}
                                            >
                                                <Cascader options={dataSupplier}/>
                                            </Form.Item>
                                        </Col>
                                        <Col className='gutter-row' span={12}>
                                            <Form.Item
                                                label='Bahan Baku'
                                                name='nama_bahan_baku'
                                                rules={[{
                                                    required: true,
                                                    message: 'Bahan Baku harus dipilih!'
                                                }]}
                                            >
                                                <Cascader options={dataBahan}/>
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                    <Row gutter={12}>
                                        <Col className='gutter-row' span={12}>
                                            <Form.Item
                                                label='Harga Penawaran'
                                                name='harga_penawaran'
                                                rules={
                                                    [{
                                                        required: true,
                                                        message: 'Harga penawaran harus diisi!'
                                                    },{
                                                        type: 'number',
                                                        min: 0,
                                                        message: 'Format harga penawaran adalah angka positif!'
                                                    }]
                                                }
                                                initialValue={dataDetail[0].harga_penawaran}
                                            >
                                                <InputNumber />
                                            </Form.Item>
                                        </Col>
                                        <Col className='gutter-row' span={12}>
                                            <Form.Item
                                                label='Ongkos Penawaran'
                                                name='ongkos_penawaran'
                                                rules={
                                                    [{
                                                        required: true,
                                                        message: 'Ongkos penawaran harus diisi!'
                                                    },{
                                                        type: 'number',
                                                        min: 0,
                                                        message: `Minimal Ongkos penawaran pembelian adalah 0`
                                                    }]
                                                }
                                                initialValue={dataDetail[0].ongkos_penawaran}
                                            >
                                                <InputNumber/>
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                    <Row gutter={12}>
                                        <Col className='gutter-row' span={12}>
                                            <Form.Item
                                                label='Kualitas Penawaran'
                                                name='kualitas_penawaran'
                                                rules={
                                                    [{
                                                        required: true,
                                                        message: 'Kualitas penawaran harus dipilih!'
                                                    }]
                                                }
                                            >
                                                <Cascader options={dataKualitas}/>
                                            </Form.Item>
                                        </Col>
                                        <Col className='gutter-row' span={12}>
                                            <Form.Item
                                                label='Stok Penawaran'
                                                name='stok_penawaran'
                                                rules={
                                                    [{
                                                        required: true,
                                                        message: 'Stok penawaran harus diisi!'
                                                    },{
                                                        type: 'number',
                                                        min: 0,
                                                        message: `Minimal stok penawaran pembelian adalah 0`
                                                    }]
                                                }
                                                initialValue={dataDetail[0].stok_penawaran}
                                            >
                                                <InputNumber/>
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                    <Row gutter={12}>
                                        <Col className='gutter-row' span={12}>
                                            <Form.Item
                                                label='Tanggal Penawaran'
                                                name='tanggal_penawaran'
                                                rules={
                                                    [{
                                                        required: true,
                                                        message: 'Tanggal penawaran harus dipilih!'
                                                    }]
                                                }
                                            >
                                                <DatePicker style={{width: '100%'}} />
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                    <Form.Item
                                        style={{marginBottom: 0, textAlign: 'right'}}
                                    >
                                        <Button type="primary" htmlType="submit">
                                            Ubah Data
                                        </Button>
                                    </Form.Item>
                                </Form>
                            ) : <div>Mengambil data...</div>
                        }
                    </Modal>
                    {/* Delete Data */}
                    <Modal
                        title='Hapus Data Stok Masuk'
                        centered
                        visible={this.state.modalDelete}
                        onCancel={this.modalDelete}
                        onOk={this.deleteData}
                    >
                        {
                            !_.isEmpty(dataDetail) ? (
                                <div>Apakah anda yakin ingin menghapus penawaran pada tanggal {dataDetail[0].tanggal_penawaran}?</div>
                            ) : <div>Mengambil data...</div>
                        }   
                    </Modal>
                </Layout>
            )
        } else {
            return <Loading />
        }
    }
}

const mapStateToProps = (state) => ({
    penawaranState: state.penawaranState,
    penawaranDetailState: state.penawaranDetailState
})

const mapDispatchToProps = (dispatch) => ({
    getPenawaranRequest: (data) => dispatch(PenawaranActions.getPenawaranRequest(data)),
    getPenawaranDetailRequest: (data) => dispatch(PenawaranDetailActions.getPenawaranDetailRequest(data)),
    addPenawaranRequest: (data) => dispatch(PenawaranActions.addPenawaranRequest(data)),
    updatePenawaranRequest: (data, id) => dispatch(PenawaranActions.updatePenawaranRequest(data, id)),
    deletePenawaranRequest: (data) => dispatch(PenawaranActions.deletePenawaranRequest(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(Penawaran)