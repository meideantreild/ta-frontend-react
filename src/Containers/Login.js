import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter, Redirect } from 'react-router-dom'
import _ from 'lodash'
import { Row, Col, Typography, Input, Button, message } from 'antd'

// redux
import LoginActions from '../ReduxSaga/Redux/LoginRedux'
import RouteActions from '../ReduxSaga/Redux/RouteRedux'

const { Title } = Typography

class Login extends Component {
    constructor (props) {
      super(props)
      this.state = {
        authState: undefined,
        username: '',
        password: '',
      }
    }

    static getDerivedStateFromProps (nextProps, prevState) {
      let updated = {}
      if (!_.isEqual(nextProps.companyState, prevState.companyState) && !_.isEmpty(nextProps.companyState)) {
          updated = { ...updated, companyState: nextProps.companyState}
      }
      if (!_.isEqual(nextProps.authState, prevState.authState) && !_.isEmpty(nextProps.authState)) {
        updated = { ...updated, authState: nextProps.authState }
        if (!_.isEmpty(nextProps.authState.err) && !nextProps.authState.success) {
          message.error(' Sorry, account not found!')
        }
      }

      if (!_.isEmpty(updated)) {
        return updated
      }
  
      return null
    }

    submitLogin = () => {
      const {username, password, access_key} = this.state
      if (_.isEmpty(username)) {
        message.warning(' Nama pengguna harus diisi!')
      }
      if (_.isEmpty(password)) {
        message.warning(' Password harus diisi!')
      }
      if (!_.isEmpty(username) && !_.isEmpty(password)) {
        const payload = {
          username: username,
          password: password
        }
        this.props.loginRequest(payload)
      }
    }
  
    componentDidUpdate () {
      const { authState } = this.state
      const payload = !authState.fetching && authState.success ? authState.data : {}
      if(!_.isEmpty(payload)) {
        this.props.routeRequest()
        message.success(` Welcome ${payload.username}`)
        return <Redirect to='/bahanbaku'/>
      }
    }

    render() {
        return (
            <div id='login'>
                <div className='content'>
                    <Row align='middle'>
                        <Col className='title-login' span={12}>
                            <Title>Sistem</Title>
                            <Title>Pendukung</Title>
                            <Title>Keputusan</Title>
                        </Col>
                        <Col className='form-login' span={12}>
                            <Input placeholder='Nama Pengguna' onChange={(e) => {this.setState({username: e.target.value})}}/>
                            <Input.Password placeholder='Kata Sandi' visibilityToggle={false} onChange={(e) => {this.setState({password: e.target.value})}}/>
                            <Button type="primary" onClick={() => this.submitLogin()}>Masuk</Button>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    authState: state.authState
})

const mapDispatchToProps = (dispatch) => ({
    loginRequest: (data) => dispatch(LoginActions.loginRequest(data)),
    routeRequest: () => dispatch(RouteActions.routeRequest())
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login))