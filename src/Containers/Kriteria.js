import React, { Component } from 'react'
import _ from 'lodash'
import { connect } from 'react-redux'
import { Table, Space, Button, Modal, Form, Row, Col, Input, InputNumber, message } from 'antd'

import Layout from '../Components/LayoutMenu'
import Loading from '../Components/Loading'
import { NumberWithCommas } from '../Utils'

// Redux
import KriteriaActions from '../ReduxSaga/Redux/KriteriaRedux'
import KriteriaDetailActions from '../ReduxSaga/Redux/KriteriaDetailRedux'

class Kriteria extends Component {
    constructor (props) {
        super(props)
        this.state = {
            kriteriaState: undefined,
            kriteriaDetailState: undefined,
            page: 1,
            idDetail: null,
            modalEdit: false,
        }
        this.columns = [
            {
                title: 'Kriteria',
                dataIndex: 'nama_kriteria',
            },
            {
                title: 'Metode',
                dataIndex: 'metode_kriteria',
                render: (value) => (
                  <Space size="middle">
                      { value === 1 ? <span>SAW</span> : <span>TOPSIS</span>}
                  </Space>
                ),
            },
            {
                title: 'Bobot',
                dataIndex: 'bobot_kriteria',
                align: 'center',
            },
            {
                title: 'Jenis',
                dataIndex: 'jenis_kriteria',
                render: (value) => (
                  <Space size="middle">
                      { value === 1 ? <span>Biaya</span> : <span>Kebutuhan</span>}
                  </Space>
                ),
            },
            {
                title: 'Aksi',
                align: 'center',
                render: (record) => (
                    <Space size="middle">
                        <a onClick={() => this.getDetail(record.id_kriteria)}>Ubah Kriteria</a>
                    </Space>
                ),
            },
        ]
    }

    static getDerivedStateFromProps (nextProps, prevState) {
        let updated = {}
        if (!_.isEqual(nextProps.kriteriaState, prevState.kriteriaState) && !_.isEmpty(nextProps.kriteriaState)) {
            updated = { ...updated, kriteriaState: nextProps.kriteriaState }
        }
        if (!_.isEqual(nextProps.kriteriaDetailState, prevState.kriteriaDetailState) && !_.isEmpty(nextProps.kriteriaDetailState)) {
            updated = { ...updated, kriteriaDetailState: nextProps.kriteriaDetailState }
        }
        if (!_.isEmpty(updated)) {
          return updated
        }
        return null
    }

    modalEdit = () => {
        this.setState(prevState => ({
          modalEdit: !prevState.modalEdit
        }))
    }

    getDetail = (e) => {
        this.setState({
            idDetail: e
        })
        this.props.getKriteriaDetailRequest(e)
        this.modalEdit()
    }

    editData = values => {
        this.setState({
            page: 1
        })
        let payload = {
            bobot_kriteria: values.bobot_kriteria
        }
        message.info('Data berhasil diubah')
        this.props.updateKriteriaRequest(payload, this.state.idDetail)
        this.modalEdit()
    }

    changePage = (e) => {
        this.props.getKriteriaRequest(e)
        this.setState({
            page: e
        })
    }

    componentDidMount () {
      const { page } = this.state
      this.props.getKriteriaRequest(page)
    }

    render () {
        const { kriteriaState, kriteriaDetailState, page } = this.state
        const data = !kriteriaState.fetching && kriteriaState.success && !_.isEmpty(kriteriaState.data) ? kriteriaState.data : {}
        const dataDetail = !kriteriaDetailState.fetching && kriteriaDetailState.success && !_.isEmpty(kriteriaDetailState.data) ? kriteriaDetailState.data : {}
        if (!kriteriaState.fetching && kriteriaState.success) {
            let sizePage = data.data.page * 10
            return (
                <Layout  parent='menu-kriteria' type='kriteria'>
                    <Table
                        style={{marginTop: 10}}
                        columns={this.columns}
                        dataSource={data.data.data}
                        pagination={{
                            defaultCurrent: page,
                            total: sizePage,
                            showSizeChanger: false,
                            onChange: this.changePage
                        }}
                    />
                    {/* Edit Data */}
                    <Modal
                        title='Ubah Data Bahan Baku'
                        centered
                        visible={this.state.modalEdit}
                        onCancel={this.modalEdit}
                        footer={null}
                    >
                        {
                            !_.isEmpty(dataDetail) ? (
                                <Form
                                    layout='vertical'
                                    labelCol={{ span: 24 }}
                                    onFinish={this.editData}
                                >
                                    <Form.Item
                                        label='Kriteria'
                                        name='nama_kriteria'
                                        initialValue={dataDetail.nama_kriteria}
                                    >
                                        <Input disabled/>
                                    </Form.Item>
                                    <Form.Item
                                        label='Metode'
                                        name='metode_kriteria'
                                        initialValue={
                                            dataDetail.metode_kriteria === 1 ? 'Metode SAW' : 'Metode TOPSIS' 
                                        }
                                    >
                                        <Input disabled/>
                                    </Form.Item>
                                    <Form.Item
                                        label='Bobot'
                                        name='bobot_kriteria'
                                        rules={
                                            [{
                                                required: true,
                                                message: 'Bobot harus diisi!'
                                            },{
                                                type: 'number',
                                                message: 'Format bobot adalah angka!'
                                            }]
                                        }
                                        initialValue={dataDetail.bobot_kriteria}
                                    >
                                        <InputNumber/>
                                    </Form.Item>
                                    <Form.Item
                                        label='Jenis'
                                        name='jenis_kriteria'
                                        initialValue={
                                            dataDetail.jenis_kriteria === 1 ? 'Biaya' : 'Keuntungan'
                                        }
                                    >
                                        <Input disabled/>
                                    </Form.Item>
                                    <Form.Item
                                        style={{marginBottom: 0, textAlign: 'right'}}
                                    >
                                        <Button type="primary" htmlType="submit">
                                            Ubah Data
                                        </Button>
                                    </Form.Item>
                                </Form>
                            ) : null
                        }
                    </Modal>
                </Layout>
            )
        } else {
            return <Loading />    
        }
    }
}

const mapStateToProps = (state) => ({
    kriteriaState: state.kriteriaState,
    kriteriaDetailState: state.kriteriaDetailState
})

const mapDispatchToProps = (dispatch) => ({
    getKriteriaRequest: (data) => dispatch(KriteriaActions.getKriteriaRequest(data)),
    getKriteriaDetailRequest: (data) => dispatch(KriteriaDetailActions.getKriteriaDetailRequest(data)),
    updateKriteriaRequest: (data, id) => dispatch(KriteriaActions.updateKriteriaRequest(data, id))
})

export default connect(mapStateToProps, mapDispatchToProps)(Kriteria)