import React from 'react'
import { PersistGate } from 'redux-persist/integration/react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'

import Routes from './Routes'
import Data from './ReduxSaga/Redux'

// Components
import Loading from './Components/Loading'

import 'antd/dist/antd.css'
import 'jquery/dist/jquery.min.js'
import './Statics/css/style.css'

render(
    <Provider store={Data.store}>
      <PersistGate loading={<Loading />} persistor={Data.persistor}>
        <ConnectedRouter history={Data.history}>
          <Routes />
        </ConnectedRouter>
      </PersistGate>
    </Provider>,
    document.getElementById('root')
  )

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();